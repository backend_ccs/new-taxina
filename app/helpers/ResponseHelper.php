<?php

namespace App\helpers\ResponseHelper;

class ResponseHelper
{
    public static function apiResponse($code ,$data,$message){

        $message['error'] = $code;
        $message['message'] = $data;
        $message['data'] = $message;
        return response()->json($message);
    }
}
?>