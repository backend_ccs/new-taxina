<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aboutUs extends Model
{
    public $table = "aboutUs";

    public static $wrap = 'aboutUs';
}
