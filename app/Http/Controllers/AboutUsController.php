<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AboutUS;
use App\Http\Resources\AboutUsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AboutUsController extends Controller
{
    public function index()
    {
        
        

        // return response()->json([
        //     'about_us' => new AboutUsResource($aboutUS),
        // ]);
        // return new AboutUsResource(response()->json([
        //     'about_us' => new AboutUsResource($aboutUS),
        // ]));
        
        return AboutUsResource::collection(AboutUS::all());
        
        //return new AboutUsResource($aboutUS);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //

        $aboutUS = AboutUS::findOrFail($id);

        return new AboutUsResource($aboutUS);
        
    }

    public function edit($id)
    {
        //
        
    }

    public function update(Request $request, $id)
    {

        $aboutUS = AboutUS::findOrFail($id);
        //
        // return $id ;
        // return new AboutUsResource($aboutUS);

        $aboutUS->name = $request->name;
        $aboutUS->time_price = $request->time_price;
        $aboutUS->present = $request->present;
        //$aboutUS->save();

        if ($aboutUS->save()) {
            return new AboutUsResource($aboutUS);
        }

        return null;
        // return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new AboutResource($About)]);
    }

    public function destroy($id)
    {
        //
    }
}
