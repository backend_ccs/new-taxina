<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\City;
use carbon\carbon;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use App\Area;
use App\Offer;

class placeController extends Controller
{
    public $message=array();

    public function show_country(Request $request){
    	if(Auth()->User()){

	     $country = \App\Country::all();

	     	if(count($country)>0){
     	 		
               $message['data'] = $country;
               $message['error'] = 0;
               $message['message'] = 'show all country';
     	 	}else{
               $message['data'] = $country;
               $message['error'] = 1;
               $message['message'] = 'no country yet';
     	 	}
     	}else{
      	     $message['data'] = \App\Country::all();;
             $message['error'] = 0;
             $message['message'] = 'show all country';
        }
       return response()->json($message);
    	    
    }
    	
    	
    	
    	
    	public function show_countryByid(Request $request){
          if(Auth()->User()){
    	         $id = $request->input('id');
    
    	         $country = \App\Country::where('id',$id)->first();
    
    	     	if( $country !=null){
	     	 	   $message['data'] = $country;
	               $message['error'] = 0;
	               $message['message'] = 'show country data';
         	 	}else{
                   $message['data'] = $country;
                   $message['error'] = 1;
                   $message['message'] = 'no country yet';
         	 	}
            }else{
          	    $message['error'] = 2;
                $message['message'] = 'this token is not provided';
            }
           return response()->json($message);
    	      
    	}
    	
    	
        public function delete_country(Request $request){
    	    if(Auth()->User()){
    
                $id = $request->input('id');
                $country = \App\Country::where('id',$id)->delete();
    	     	
         	    if($country == true){
         	 	    $message['error'] = 0;
                    $message['message'] = 'this country is deleted successfully';
     	    	}else{
                   $message['error']=1;
                   $message['message']='error in delete city';
         	   	}
             }else{
    
          	    $message['error'] = 2;
                $message['message'] = 'this token is not provided';
             }
       return response()->json($message);
    	   
    	}
    	
    	
    	public function insert_country(Request $request){
         if(Auth()->User()){
    
    	          
    	      $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
              
               $name = $request->input('name');
     
              $select = \App\Country::where('name',$name)->first();
    
              if($select !=null){
                   $message['error'] = 1;
                   $message['message']=' city is already exist';
                  
              }else{
    	         
    	         $country = new \App\Country;
    	         $country->name = $name;
    	         $country->created_at = $dateTime;
    	         $country->save();
    	         
    	     	if($country == true){
         	 		
                   $message['error'] = 0;
                   $message['message'] = ' country is add successfully';
         	 	}else{
                   $message['error'] = 1;
                   $message['message'] = 'error in insert country';
         	 	}
              }
            }else{
    
          	    $message['error'] = 2;
                $message['message'] = 'this token is not provided';
             }  
         return response()->json($message);
    	       
    	}
    	
    	
    	public function update_country(Request $request){
         if(Auth()->User()){
             
    	      $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
               $id = $request->input('id');   
    	       $name = $request->input('name');
          
              $select = \App\Country::where([['name',$name],['id','!=',$id]])->first();
    
              if($select !=null){
                   $message['error'] = 1;
                   $message['message'] = ' country is already exist';
                  
              }else{
    	         
    	         $update = \App\Country::where('id' , $id)->update([
                                                              'name' => $name,
                                                              'updated_at' => $dateTime
                                                
                                                	         ]);
                                        	  
    	     	if($update == true){
         	 	   $message['error'] = 0;
                   $message['message'] = 'update country successfully';
         	 	}else{
                   $message['error'] = 1;
                   $message['message'] = 'error in update country';
         	 	}
              }
            }else{
    
          	    $message['error'] = 2;
                $message['message'] = 'this token is not provided';
             }  
           return response()->json($message);
    	       
    	}


  public function show_cities(Request $request){
	if(Auth()->User()){

            $country_id = $request->input('country_id');    
	         $city = City::where('country_id' , $country_id)->get();

	     	if(count($city)>0){
     	 		
               $message['data']=$city;
               $message['error']=0;
               $message['message']='show all cities in this country';
     	 	}else{
               $message['data']=$city;
               $message['error']=1;
               $message['message']='no cities yet';
     	 	}
     	 }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
     return response()->json($message);
	    
	}
	
	public function show_allcities(Request $request){
	if(Auth()->User()){

	         $city = City::all();

	     	if(count($city)>0){
     	 		
               $message['data']=$city;
               $message['error']=0;
               $message['message']='show all cities in this country';
     	 	}else{
               $message['data']=$city;
               $message['error']=1;
               $message['message']='no cities yet';
     	 	}
     	 }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
     return response()->json($message);
	    
	}
	
	
	
	public function show_cityByid(Request $request){
      if(Auth()->User()){
	         $id=$request->input('id');

	         $city = City::where('id',$id)->first();

	     	if( $city !=null){
	     	 	   $message['data']=$city;
	               $message['error']=0;
	               $message['message']='show  city';
     	 	}else{
              $message['data']=$city;
              $message['error']=1;
               $message['message']='no city yet';
     	 	}
        }else{
      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
        }
       return response()->json($message);
	      
	}
	
	
    public function delete_cities(Request $request){
	    if(Auth()->User()){

            $id=$request->input('id');
            $city=City::where('id',$id)->delete();
	     	
     	    if($city == true){
     	 	    $message['error']=0;
                $message['message']= 'city is deleted successfully';
 	    	}else{
               $message['error']=1;
               $message['message']='error in delete city';
     	   	}
         }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
   return response()->json($message);
	   
	}
	
	
	public function insert_city(Request $request){
     if(Auth()->User()){

	          
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
           $name=$request->input('name');
 
          $select = City::where('name',$name)->first();

          if($select !=null){
               $message['error']=4;
               $message['message']=' city is already exist';
              
          }else{
	         
	         $city = new City;
	         $city->name = $name;
	         $city->country_id = $request->input('country_id');
	         $city->created_at = $dateTime;
	         $city->save();
	         
	     	if($city == true){
     	 		
               $message['error']=0;
               $message['message']='insert city success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in insert city';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
	       
	}
	
	
	public function update_city(Request $request){
     if(Auth()->User()){
         
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
      
           $id=$request->input('id');   
	       $name=$request->input('name');
      
          $select = City::where([['name',$name],['id','!=',$id]])->first();

          if($select !=null){
              
               $message['error']=4;
               $message['message']=' city exist';
              
          }else{
	         
	         $update=City::where('id',$id)->update([
                                                  'name'=>$name,
                                                  'updated_at'=>$dateTime
                                    
                                    	         ]);
                                    	  
	     	if($update == true){
     	 		
               $message['error']=0;
               $message['message']='update city success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in updtea city';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
       return response()->json($message);
	       
	}
	
	
	
	//********************************************* area *******************************//
	
	
	public function show_Area(Request $request){
       if(Auth()->User()){
    	     $id=$request->input('city_id');
	         $area=Area::select('id','name','created_at')->where('city_id',$id)->get();

	     	if(count($area)>0){
     	       $message['data']=$area;
               $message['error']=0;
               $message['message']='show all Areas in city';
     	 	}else{
               $message['data']=$area;
               $message['error']=1;
               $message['message']='no area yet';
     	 	}
     	 }else{
      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
       return response()->json($message);
	 }


     public function show_areaByid(Request $request){
	      if(Auth()->User()){
	         $id=$request->input('id');

	         $area=Area::select('id','name')->where('id',$id)->first();

	     	if( $area !=null){
     	 	   $message['data']=$area;
               $message['error']=0;
               $message['message']='show  area';
     	 	}else{
               $message['data']=NULL;
               $message['error']=1;
               $message['message']='no area yet';
     	 	}
        }else{
      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
        }

        return response()->json($message);
	}
	
	
	public function delete_area(Request $request){
	  if(Auth()->User()){

        $id=$request->input('id');
        $area=Area::where('id',$id)->delete();
     
     	if($area == true){
            $message['error']=0;
            $message['message']='delete area success';
 	 	}else{
        
          $message['error']=1;
           $message['message']='error in delete area';
 	 	}
     }else{

  	    $message['error'] = 2;
        $message['message'] = 'this token is not provided';
     }

 	    return response()->json($message);
	   
	}
	
	
	public function insert_area(Request $request){
	     if(Auth()->User()){
    
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
 
          $name=$request->input('name');
	      $city=$request->input('city_id');
 
          $select=Area::where('name',$name)->first();

          if($select !=null){
              
               $message['error']=4;
               $message['message']=' area exist';
              
          }else{
	         
	         $insert=new Area;
	         $insert->name=$name;
	         $insert->city_id=$city;
	         $insert->created_at=$dateTime;
	         $insert->save();
	         
	     	if($insert == true){
     	 		
              $message['error']=0;
               $message['message']='insert area success';
     	 	}else{
              $message['error']=1;
               $message['message']='error in insert area';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
            return response()->json($message);
	}
	
	
	public function update_area(Request $request){
	  if(Auth()->User()){

	          
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
      
           $id=$request->input('id');   
	       $name=$request->input('name');
     
          $select=Area::where([['name',$name],['id','!=',$id]])->first();

          if($select !=null){
               $message['error']=4;
               $message['message']=' area exist';
          }else{
	         $update=Area::where('id',$id)->update([
                                              'name'=>$name,
                                              'updated_at'=>$dateTime
                                
                                	         ]);
	         
	     	if($update == true){
               $message['error']=0;
               $message['message']='update area success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in updtea area';
     	 	}
          }
        }else{
      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
	       
	}


//************************************** user country**************************/


    public function add_user_country(Request $request){
        if(auth()->User()){
            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "تم أضافه هذه المدينه اليك";
                $msg_error  = "يوجد خطأ, حاول مره أخرى";
                $msg_2   =  "تم تعديل مدينتك بنجاح";
            }else{
                $msg_data = "A new country is add";
                $msg_error = "Email or Password is wrong!!";
                $msg_2 = "Your country is updated successfully";
            }
            
    	    $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
            
            $country_id = $request->input('country_id');
            
            $check = \App\User_country::where('user_id' , auth()->User()->id)->value('id');
            
            if($check != NULL){
                $update_country = \App\User_country::where('user_id' , auth()->User()->id)->update(['country_id' => $country_id ]);
                
                if($update_country == true){
                    $message['data'] = \App\User::where('id',Auth()->User()->id)->first();
                    $message['error'] = 0;
                    $message['message'] = $msg_2;
                }
            }else{
                
                $add = new \App\User_country;
                $add->User_id = auth()->User()->id;
                $add->country_id = $country_id;
                $add->created_at = $dateTime;
                $add->save();
                
                if( $add == true){
                    $message['data'] = \App\User::where('id',Auth()->User()->id)->first();
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['data'] = \App\User::where('id',Auth()->User()->id)->first();
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }
            
        }else{
      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
    }
    
    
    
    
    
    
}
?>