<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Carbon\Carbon;

use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use App\Special_trip;

define( 'API_ACCESS_KEY', 'AAAAx0JkJkg:APA91bF4mFym1nM37kLnaTqgDnUVBqbhp41Xx5GH978IoJUT9MSLh03T6DxfB98R2V5SXgeiEQ0LkB0ZGZ7cOv6m8JSaxWIu-BJOYceF6r1PFxG022AfwFLP-l_T86l4aUjyqONFZ0tt');

class specialTripController extends Controller
{
    public $message = array();

    public function add_specialtrip(Request $request){
        if(Auth()->User()){
             $msg_data ="";
            $msg_error ="";
            $msg_token ="";
            $msg_error2="";
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting =='ar'){
                    $msg_data =  "تم أضافه الرحلة ";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";         
                    $msg_error2 = "انت لا تمتلك ميكروباس ";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "special trip is add successfully ";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 = "you don't have Bus, to make special trip";;
                    $msg_token = "Token is not provided";   
                }

            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
        
            $check = \App\User_car::where([['user_id', auth()->user()->id] , ['type_id' , '2']])->first();
            
            if($check  != NULL){
                $add = new \App\Special_trip;
    
                // $add->start_location = $request->input('start_location');
                $add->start_Latitude = $request->input('start_Latitude');
                $add->start_Longitude = $request->input('start_Longitude');
                // $add->end_location = $request->input('end_location');
                $add->end_Latitude = $request->input('end_Latitude');
                $add->end_Longitude = $request->input('end_Longitude');
                $add->from_areaId = $request->input('from_areaId');
                $add->to_areaId = $request->input('to_areaId');
                $add->seat_count = $request->input('seat_count');
                $add->date = $request->input('date');
                $add->from_time = $request->input('from_time');
                $add->to_time = $request->input('to_time');
                $add->price = $request->input('price');
                $add->user_id = auth()->User()->id;
                $add->is_driver = 1;
                $add->state = 'wait';
                $add->created_at = $dateTime;
                $add->save();
    
    
                $each_user = $request->input('price') /$request->input('seat_count');
                
                $seats = array();
                
                for($x = 1; $x<= $request->input('seat_count'); $x++){
                    $seats[] = [
                        "specialTrip_id" => $add->id,
                        "driver_id" => auth()->User()->id,
                        "seat_num" => $x,
                        "money"=> $each_user,
                        "user_id" => NULL,
                        "is_ride" => 0,
                        "created_at" => $dateTime,
                    ];
                }
    
                $add_seats = \App\Car_seat::insert($seats);
    
               $code = rand(1000,9999);
        
              $update_code = \App\Special_trip::where('id', $add->id)->update(['code' => $code]);
    
    
                if( $add ==true && $add_seats == true){
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }



    public function show_specialTrip(Request $request){

        if(Auth()->User()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "الرحلات المخصوصة ";
                    $msg_error  = "لايوجد رحلات ";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "all special trips";
                    $msg_error = "No special Trip";
                    $msg_token = "Token is not provided";   
                }


            $get_data = \App\Special_trip::where([['is_driver', '1'],['state' , 'wait']])->get();

            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] =$msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
    
    
    public function show_trip_seats(Request $request){
        if(Auth()->User()){

            
            $trip_id = $request->input('trip_id');

            $data = \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price','user_car.car_name','user_car.plate', 'user_car.color')
                                     ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                     ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                     ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                     ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                     ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                     ->where('special_trip.id' , $trip_id)->first();
                                     
            $get_data = \App\Car_seat::select('id','seat_num','user_id','is_ride')->where('specialTrip_id' , $trip_id)->get();
                        
            $avaliable = \App\Car_seat::select('id')->where([['specialTrip_id' , $trip_id],['is_ride' , '0']])->count();
            
            $ride = \App\Car_seat::select('id')->where([['specialTrip_id' , $trip_id],['is_ride' , '1']])->count();
            $block = \App\Car_seat::select('id')->where([['specialTrip_id' , $trip_id],['is_ride' , '2']])->count();
        
            $start = Carbon::parse($data->from_time);
            $end = Carbon::parse($data->to_time);

            $durationTime = $end->diff($start)->format('%H:%I');
            
            $data['seats'] = $get_data;
            $data['avaliable_num'] = $avaliable;
            $data['ride_num'] = $ride;
            $data['block'] = $block;
            $data['trip_time'] =   $durationTime; 

            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "أماكن العربية ";
                    $msg_error  = "لا يوجد رحلة";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "car seats";
                    $msg_error = "no trip";
                    $msg_token = "Token is not provided";   
                }


            if( count($get_data)>0 ){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] =$msg_data;
            }else{
                $message['data'] = $data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            } 
        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
    
    
    public function join_trip(Request $request){
        if(auth()->user()){

            $created_at = carbon::now()->toDateTimeString();
            $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $trip_id = $request->input('trip_id');
            $seat_id = $request->input('seat_id');

            $msg_data ="";
            $msg_error ="";
            $msg_error2 ="";
            $msg_error3 = "";
            $update = "";
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "تم حجز كرسى فى هذة الرحلة";
                    $msg_error  = "من فضلك أدخل جميع البيانات";
                    $msg_error2 = "يوجد خطأ، حاول مره أخرى";
                    $msg_error3 = "تم الاغاء الكرسى";
                }else{
                    $msg_data =  "you are joing the trip";
                    $msg_error = "please fill all the fields"; 
                    $msg_error2 = "error, please try again";   
                    $msg_error3 = "your join is deleted";
                }
           
                $get_driver = \App\Special_trip::where('id' , $trip_id)->value('user_id');
                
                $user = auth()->User()->id;
                
                $get_firebase = \App\User::where('id' , $get_driver)->value('firebase_token');
            
             define( 'API_ACCESS_KEY12', 'AAAAx0JkJkg:APA91bF4mFym1nM37kLnaTqgDnUVBqbhp41Xx5GH978IoJUT9MSLh03T6DxfB98R2V5SXgeiEQ0LkB0ZGZ7cOv6m8JSaxWIu-BJOYceF6r1PFxG022AfwFLP-l_T86l4aUjyqONFZ0tt');
    
    
                        #prep the bundle
                             $msg = array
                                  (
                        		'body' 	=> "تم حجز اماكن فى رحلتك" ,
                        		'title'	=>  "Taxina Trip",
                                'click_action' => "2",
                                
                                  );
                        	$fields = array
                        			(
                        				'to'		=> $get_firebase,
                                        'data' => $m = array(
                                                    'redirect_id' => "$trip_id",
                                                    'type' => "$user",
                                                    'status' => "2",
                                                ),
                        				'notification'	=> $msg
                        			);
                        	
                        	                 
    
                        	$headers = array
                        			(
                        				'Authorization: key=' . API_ACCESS_KEY12,
                        				'Content-Type: application/json'
                        			);
                        #Send Reponse To FireBase Server	
                        		$ch = curl_init();
                        		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        		curl_setopt( $ch,CURLOPT_POST, true );
                        		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        		$result = curl_exec($ch );
                        		//echo $result;
                        		curl_close( $ch );
                        		
                    		
                    			
                        		$add_notify = new \App\Notification;
                            	
                            	$add_notify->body = "تم حجز اماكن فى رحلتك" ;
                            	$add_notify->title =  "Taxina Trip";
                            	$add_notify->click_action = 2;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 2;
                            	$add_notify->status = 2;
                            	$add_notify->user_id = $get_driver;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                         
            if( $request->has('trip_id') && $request->has('seat_id')){
                
                 foreach(explode(',', $request->input('seat_id')) as  $seat){
                               
                               
                    $check = \App\Car_seat::where([ ['specialTrip_id' , $trip_id], ['seat_num' , $seat] , ['user_id' , auth()->user()->id] , ['state' , 'need']])->value('id');
                    
                    if( $check != NULL){
                         
                         $delete = \App\Car_seat::where([['specialTrip_id' , $trip_id], ['seat_num' , $seat] , ['user_id' , auth()->user()->id] ])->update([   'user_id' => 0,
                                                                                                                                                               'is_ride' => 0,
                                                                                                                                                               'state'   => ""]);  
                                                                                                                                                           

                    }else{
                
                               
                        $update = \App\Car_seat::where([['specialTrip_id' , $trip_id], ['seat_num' , $seat]])->update([ 'user_id' => auth()->user()->id , 
                                                                                                                    'is_ride' => 1 , 
                                                                                                                    'state'   => 'need']);

                    }   
                    
                    if( $update == true){
                        $message['error'] = 0;
                        $message['message'] = $msg_data;
                    }elseif( $delete == true){
                          $message['error'] = 0;
                          $message['message'] = $msg_error3;
                    }else{
                        $message['error'] = 1;
                        $message['message'] = $msg_error2;
                    }
                }

                
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    public function search_trip(Request $request){
        if(auth()->User()){
            

            $from_areaId = $request->input('from_areaId');
            $to_areaId = $request->input('to_areaId');
            $date = $request->input('date');
            
            $data =  \App\Special_trip::select('special_trip.id', 'special_trip.start_Latitude', 'special_trip.start_Longitude', 'special_trip.end_Latitude', 'special_trip.end_Longitude','special_trip.from_areaId', 'fromArea.name as from_area','fromCity.name as fromcity_name', 'special_trip.to_areaId', 'toArea.name as to_area','toCity.name as tocity_name' ,'seat_count','date', 'special_trip.from_time', 'special_trip.to_time', 'special_trip.price','special_trip.user_id', 'is_driver','state','special_trip.created_at')
                                        ->join('area as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                        ->join('area as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                        ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                        ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                        ->where([['from_areaId' , $from_areaId] , ['to_areaId' , $to_areaId] , ['date' ,$date ], ['state' , 'wait']])->get();
            
            
                
                $msg_data ="";
                $msg_error ="";
                $msg_token ="";
    
                $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                    
                    if($check_setting == 'ar'){
                        $msg_data =  "جميع الرحلات";
                        $msg_error  = " لاتوجد رحله تبدا من هنا";
                    }else{
                        $msg_data =  " all trip";
                        $msg_error = "No Trip starts from this place"; 
                    }
                
                if( count($data) >0 ){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            
            }else{
                $message['data'] = $data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
     public function show_UpComingdriver_specialTrip(Request $request){

        if(Auth()->User()){

            
            $get_data = \App\Trip::where([['status' , 'need'], ['driver_id' , auth()->user()->id], ['type' , '1'] ])->get();
            
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "رحلاتك المخصوصة ";
                    $msg_error  = "لايوجد رحلات ";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "all special trips";
                    $msg_error = "No special Trip";
                    $msg_token = "Token is not provided";   
                }
            
            
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    public function upcomingDriver_Bus(Request $request){
        if(auth()->User()){
        
            $data = array();
            
            $get_data = \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price','user_car.car_name','user_car.plate', 'user_car.color')
                                     ->leftjoin('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                     ->leftjoin('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                     ->leftjoin('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                     ->leftjoin('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                     ->leftjoin('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                     ->where([['is_driver', '1'],['special_trip.state' , 'wait'], ['special_trip.user_id' , auth()->user()->id]])->get();
            
            
            $total_earned = DB::select("SELECT count(id) as count ,sum(price) as earned FROM `trip` WHERE MONTH(CURRENT_DATE()) and driver_id = ?",[auth()->user()->id]);

            foreach( $get_data as $dd){
               
               
             $start = Carbon::parse($dd->from_time);
             $end = Carbon::parse($dd->to_time);

             $durationTime = $end->diff($start)->format('%H:%I');
               
               array_push($data, (object)array(
                    "id" => $dd->id,
                    "from_areaId" => $dd->from_areaId,
                    "fromcity_name" => $dd->fromcity_name,
                    "from_area" => $dd->from_area,
                    "to_areaId" => $dd->to_areaId,
                    "tocity_name" => $dd->tocity_name,
                    "to_area" => $dd->to_area,
                    "seat_count" => $dd->seat_count,
                    "date" => $dd->date,
                    "from_time" => $dd->from_time,
                    "to_time" => $dd->to_time,
                    "price" => $dd->price,
                    "car_name" => $dd->car_name,
                    "plate" => $dd->plate,
                    "color" => $dd->color,
                    "trip_time" => $durationTime,
                    ));
            }
            
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "رحلاتك المخصوصة ";
                    $msg_error  = "لايوجد رحلات ";
                }else{
                    $msg_data =  "all special trips";
                    $msg_error = "No special Trip";
                }
            
            
            if( count($data)>0 ){
                $message['data'] = $data;
                $message['trip_count'] = $total_earned[0]->count;
                $message['earned'] = $total_earned[0]->earned;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $data;
                $message['trip_count'] = $total_earned[0]->count;
                $message['earned'] = $total_earned[0]->earned;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }  else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);  
    }
    
    
     public function completedDriver_Bus(Request $request){
        if(auth()->User()){
        
            $data = array();
            
            $get_data = \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price','user_car.car_name','user_car.plate', 'user_car.color')
                                     ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                     ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                     ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                     ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                     ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                     ->where([['special_trip.is_driver', '1'],['special_trip.state' , 'end'], ['special_trip.user_id' , auth()->user()->id]])->get();
            
             foreach( $get_data as $dd){
               
               
             $start = Carbon::parse($dd->from_time);
             $end = Carbon::parse($dd->to_time);

             $durationTime = $end->diff($start)->format('%H:%I');
               
               if( $dd != NULL){
               array_push($data, (object)array(
                    "id" => $dd->id,
                    "from_areaId" => $dd->from_areaId,
                    "fromcity_name" => $dd->fromcity_name,
                    "from_area" => $dd->from_area,
                    "to_areaId" => $dd->to_areaId,
                    "tocity_name" => $dd->tocity_name,
                    "to_area" => $dd->to_area,
                    "seat_count" => $dd->seat_count,
                    "date" => $dd->date,
                    "from_time" => $dd->from_time,
                    "to_time" => $dd->to_time,
                    "price" => $dd->price,
                    "car_name" => $dd->car_name,
                    "plate" => $dd->plate,
                    "color" => $dd->color,
                    "trip_time" => $durationTime,
                    ));
               }
            }
    
             $total_earned = DB::select("SELECT count(id) as count ,sum(price) as earned FROM `trip` WHERE MONTH(CURRENT_DATE()) and driver_id = ?",[auth()->user()->id]);

            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "رحلاتك المنتهية ";
                    $msg_error  = "لايوجد رحلات ";
                }else{
                    $msg_data =  "all ended special trips";
                    $msg_error = "No special Trip";
                }
            
            
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['trip_count'] = $total_earned[0]->count;
                $message['earned'] = $total_earned[0]->earned;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['trip_count'] = $total_earned[0]->count;
                $message['earned'] = $total_earned[0]->earned;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }  else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);  
    }
    
    
     public function Block_trip(Request $request){
        if(auth()->user()){

             $msg_data ="";
            $msg_error ="";
            $msg_token ="";
            $msg_error2 ="";
            $msg_error3 ="";
            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "تم إغلاق هذا الكرسى  فى الرحلة";
                    $msg_error  = "يوجد خطأ، حاول مره اخرى";
                    $msg_error2 = "لا يمكنك إغلاق هذا الكرسى";
                    $msg_error3 ="من فضلك أدخل جميع البيانات";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "you are joing the trip";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 ="you can't block this seat";
                    $msg_error3 ="please fill all the fields";   
                    $msg_token = "Token is not provided";   
                }

            $trip_id = $request->input('trip_id');
            $seat_id = $request->input('seat_id');

            if( $request->has('trip_id') && $request->has('seat_id')){
                
                $check = \App\Car_seat::where([['specialTrip_id' , $trip_id], ['id' , $seat_id]])->value('is_ride');
                
                if( $check == "0"){
                    
                    
                    $update = \App\Car_seat::where([['specialTrip_id' , $trip_id], ['id' , $seat_id]])->update([ 'user_id'=> auth()->user()->id,
                                                                                                                 'is_ride' => 2,
                                                                                                                 'state' => 'accept']);
    
                    if( $update == true){
                        $message['error'] = 0;
                        $message['message'] = $msg_data;
                    }else{
                        $message['error'] = 1;
                        $message['message'] = $msg_error;
                    
                    }
                    
                }else{
                    $message['error'] = 1;
                    $message['message'] = $msg_error2;
            
                }
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error3;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
    
    public function start_specialTrip(Request $request){
        if(Auth()->user()){
             
            $msg_data ="";
            $msg_error ="";
            $msg_error2 ="";
            $msg_error3 = "";$msg_error4 = "";
            $msg_token ="";
            
            $created_at = carbon::now()->toDateTimeString();
             $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "الرحله بدأت ";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى ";
                    $msg_error2 = "انت معك رحله بالفعل";
                    $msg_error3 = "انت لا تملك اتوبيس";
                    $msg_error4 = "لام يتم حجز كراسى فى رحلتك";
                }else{
                    $msg_data =  "special trip is startes";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 ="you are busy now";
                    $msg_error3 = "you don't have bus";
                    $msg_error4 = "no one joined ur trip";   
                }           
            $special_tripID = $request->input('specialTrip_id');

            $check = \App\User_car::where('user_id' , auth()->User()->id)->value('type_id');
            
            

            if($check == '2'){
                
                $driver_busy = \App\User::where('id' , auth()->User()->id)->value('is_busy');
            
                if($driver_busy == '0'){
                    
                    $update = \App\Special_trip::where('id' , $special_tripID)->update(['state' => 'start']);
                    
                    $update_busy = \App\User::where('id' , Auth()->User()->id)->update(['is_busy' =>'1']);
                    
                    if($update == true && $update_busy == true){
                        
                        $check_accept = \App\Car_seat::where([['specialTrip_id' ,$special_tripID ] ,['is_ride' , '1']])->value('is_ride');
                        if($check_accept == '1'){
                            
                        
                        
                        $updat_carsets = \App\Car_seat::where([['specialTrip_id' ,$special_tripID ] ])->update(['type' => 'start']);
                        
                        
                        $get_users = \App\Car_seat::select('user_id')->where('specialTrip_id' , $special_tripID)->distinct()->get();
                
                        foreach($get_users as $id){

                            $get_firebase = \App\User::where('id' , $id->user_id)->value('firebase_token');
            
                           if($get_firebase != NULL){     
    
        
                            #prep the bundle
                                 $msg = array
                                      (
                            		'body' 	=> "تم بدأ الرحلة" ,
                            		'title'	=>  "Taxina Trip",
                                    'click_action' => "2"
                                      );
                            	$fields = array
                            			(
                            				'to'		=> $get_firebase,
                                            'data' => $m = array(
                                                         'redirect_id'  => "$special_tripID",
                                                         'type' => "2",
                                                         'status' => "2",
                                                    ),
                            				'notification'	=> $msg
                            			);
                            	
                            	                 
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                            		
                            
                            	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body = "تم بدأ الرحلة" ;
                            	$add_notify->title =  "Taxina Trip";
                            	$add_notify->click_action = 2;
                            	$add_notify->redirect_id = $special_tripID;
                            	$add_notify->type = 2;
                            	$add_notify->status = 2;
                            	$add_notify->user_id = $id->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            	
                            	
                           }
                        }
                        
                        
                        $message['error'] = 0;
                        $message['message'] = $msg_data;
                    
                        }else{
                            $update = \App\Special_trip::where('id' , $special_tripID)->update(['state' => 'wait']);
                    
                            $update_busy = \App\User::where('id' , Auth()->User()->id)->update(['is_busy' =>'0']);
                    
                            $message['error'] = 1;
                            $message['message'] = $msg_error4;
                    }
                    }else{
                        $message['error'] = 1;
                        $message['message'] = $msg_error;
                    }
                }else{
                    $message['error'] = 1;
                    $message['message'] =$msg_error2;
                }
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error3;
            }
                            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    public function end_specialTrip(Request $request){
        if(auth()->User()){
            
        $created_at = carbon::now()->toDateTimeString();
        $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "الرحله انتهت ";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى ";
                }else{
                    $msg_data =  "special trip is ended";
                    $msg_error = "there is an error, please try again";
                }    
                
            $special_tripID = $request->input('specialTrip_id');

            $company_wallet = \App\aboutUs::select('bus_present' ,'wallet' )->where('id' , '1')->first();

            $trip_price = \App\Special_trip::where('id' , $special_tripID)->value('price');
            
            $get_driverWallet = \App\User::where('id', auth()->User()->id)->value('wallet');
            
            
            $company_money =   ($trip_price * $company_wallet->bus_present) / 100;
            
            $total_company = $company_wallet->wallet + $company_money;
            
            $update_companyWallet = \App\aboutUs::where('id' , '1')->update(['wallet' => $total_company]);
            
            $driver_money = $trip_price - $company_money;
            
            $total_driver = $get_driverWallet + $driver_money;
            
            
            $update_driverWallet = \App\User::where('id' , auth()->User()->id)->update(['wallet' => $total_driver ]);
            
            $update = \App\Special_trip::where('id' ,$special_tripID)->update(['state' => 'end']);
                                
            $update_busy = \App\User::where('id' , Auth()->User()->id)->update(['is_busy' =>'0']);

            if($update == true && $update_busy == true){
                
                $updat_carsets = \App\Car_seat::where('specialTrip_id' ,$special_tripID )->update(['type' => 'end']);
                        
                        
                $get_users = \App\Car_seat::select('user_id')->where('specialTrip_id' , $special_tripID)->distinct()->get();
        
                foreach($get_users as $id){

                    $get_firebase = \App\User::where('id' , $id->user_id)->value('firebase_token');
    
                if( $get_firebase != NULL){
 

                
                	
                #prep the bundle
                     $msg = array
                          (
                		'body' 	=> "تم أنتهاء الرحلة",
                		'title'	=>  "Taxina Trip",
                        'click_action' => "3"
                          );
                	$fields = array
                			(
                				'to'		=> $get_firebase,
                                'data' => $mm = array(
                                             'redirect_id'  => "$special_tripID",
                                             'type' => "2",
                                             'status' => "3",
                                        ),
                				'notification'	=> $msg
                			);
                            		                 

                	$headers = array
                			(
                				'Authorization: key=' . API_ACCESS_KEY,
                				'Content-Type: application/json'
                			);
                #Send Reponse To FireBase Server	
                		$ch = curl_init();
                		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                		curl_setopt( $ch,CURLOPT_POST, true );
                		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                		$result = curl_exec($ch );
                		//echo $result;
                		curl_close( $ch );
                
                    	$add_notify = new \App\Notification;
                            	
                    	$add_notify->body = "تم أنتهاء الرحلة";
                    	$add_notify->title =  "Taxina Trip";
                    	$add_notify->click_action = 3;
                    	$add_notify->redirect_id = $special_tripID;
                    	$add_notify->type = 2;
                    	$add_notify->status = 3;
                    	$add_notify->user_id = $id->user_id;
                    	$add_notify->created_at = $dateTime;
                    	$add_notify->save();
                }
                } 
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
   // public function show_tripWait_
    
    public function search_specialTrip(Request $request){
        
        if( auth()->User()){
            
             $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "الرحله القادمة ";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى ";
                }else{
                    $msg_data =  "upcoming special trip ";
                    $msg_error = "there is an error, please try again";
                }  
            
            $date_from = $request->input('date_from');
            $date_to = $request->input('date_to');
            
            
            $get_data = \App\Special_trip::select('special_trip.id','special_trip.start_Latitude', 'special_trip.start_Longitude', 'special_trip.end_Latitude', 'special_trip.end_Longitude','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price','user_car.car_name','user_car.plate', 'user_car.color')
                                     ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                     ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                     ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                     ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                     ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                     ->where([['special_trip.user_id' , auth()->User()->id],['special_trip.state' , 'wait']])
                                     ->whereBetween('special_trip.created_at' , [$date_from , $date_to])->get();    
                                     
           if( count($get_data) >0 ){
               $message['data'] = $get_data;
               $message['error'] = 0;
               $message['message'] = $msg_data;
           }else{
               $message['data'] = $get_data;
               $message['error'] = 1;
               $message['message'] = $msg_error;
           }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    public function show_users_specialTrip(Request $request){
        if(auth()->User()){
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "جميع الركاب";
                    $msg_error  = "لا يوجد ركاب" ;
                }else{
                    $msg_data =  "all users seats ";
                    $msg_error = "No user seats";
                }
            
            $trip_id = $request->input('trip_id');
            
            $get_data = \App\Car_seat::select('car_seats.id', 'seat_num' , 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.gender', 'users.image', 'users.phone', 'money', 'is_ride' )
                                     ->join('users' , 'car_seats.user_id'  ,'=' ,'users.id')
                                     ->where('car_seats.specialTrip_id' , $trip_id )->get();
                                     
         
            $all = \App\Car_seat::select('id','seat_num','is_ride')->where('specialTrip_id' , $trip_id)->get();
                        
            $avaliable = \App\Car_seat::select('id')->where([['specialTrip_id' , $trip_id],['is_ride' , '0']])->count();
            
            $ride = \App\Car_seat::select('id')->where([['specialTrip_id' , $trip_id],['is_ride' , '1']])->count();
            $block = \App\Car_seat::select('id')->where([['specialTrip_id' , $trip_id],['is_ride' , '2']])->count();
        
                                     
                                     
            if( count($get_data)>0){
                $message['data'] = $get_data;
                $message['avaliable_num'] = $avaliable;
                $message['ride_num'] = $ride;
                $message['block'] = $block;
                $message['type'] = \App\Special_trip::where('id' , $trip_id)->value('state');
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['avaliable_num'] = $avaliable;
                $message['ride_num'] = $ride;
                $message['block'] = $block;
                $message['type'] = \App\Special_trip::where('id' , $trip_id)->value('state');
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
                      
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    public function show_user_seats(Request $request){
        if(auth()->User()){
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "اماكن هذا الراكب";
                    $msg_error  = "لا يوجد ركاب" ;
                }else{
                    $msg_data =  "‘User seats";
                    $msg_error = "No user seats";
                }
            
              
            $trip_id = $request->input('trip_id');
            $user_id = $request->input('user_id');
            
            $get_data = \App\Car_seat::select('id','seat_num','money', 'is_ride')->where([['specialTrip_id' , $trip_id] , ['user_id' , $user_id]])->get();
            
            if( count( $get_data) >0){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }



    public function show_tripUser_seats(Request $request){
        if(auth()->User()){
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "اماكن هذا الراكب";
                    $msg_error  = "لا يوجد ركاب" ;
                }else{
                    $msg_data =  "‘User seats";
                    $msg_error = "No user seats";
                }
            
              
            $trip_id = $request->input('trip_id');
            $user_id = $request->input('user_id');
            
            $data = \App\Car_seat::select('id','seat_num', 'money', 'user_id', 'is_ride')
                                         ->where([['specialTrip_id' , $trip_id]])->get();
                                                
            
            $get_data = \App\Car_seat::select( 'car_seats.user_id', 'car_seats.is_ride','users.image', 'users.first_name', 'users.last_name', 
                                                'special_trip.date', 'special_trip.price', 'special_trip.from_areaId', 'fromArea.name as from_area' , 'special_trip.to_areaId',  'toArea.name as to_area')
                                     ->leftJOIN('special_trip' ,  'car_seats.specialTrip_id', '=', 'special_trip.id')
                                     ->leftJOIN('users' , 'car_seats.user_id' ,'=' ,'users.id')
                                     ->leftJOIN('area as fromArea' , 'special_trip.from_areaId' ,'=', 'fromArea.id')
                                     ->leftJOIN('area as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                     ->where([['specialTrip_id' , $trip_id] , ['car_seats.user_id' , $user_id]])->first();
            
            if( count( $data) >0){
                $message['data'] = $data;
                $message['details'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    public function rate_specialTrip(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_error2="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم تقيم الرحلة";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "منفضلك ادخل البييانات";
                }else{
                    $msg_data = "Trip is rating";
                    $msg_error = "there is an error, please try again";
                    $msg_error2= "please enter all the fields";
                }
                
            $specialTrip_id = $request->input('specialTrip_id');
            
            $rate = $request->input('rate');

            if( $request->has('rate') && $rate != NULL && $request->has('specialTrip_id') && $specialTrip_id != NULL){

                 $update = \App\Car_seat::where([['specialTrip_id' , $specialTrip_id], ['user_id' , auth()->User()->id]])->update(['rate' => $rate]);
            
                 if( $update == true){
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    public function show_specialTrip_need(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "جمبع الرحلات المخصوصه لك";
                    $msg_error  = "لا يوجد رحلات " ;
                }else{
                    $msg_data =  "all specail trip add for yoع";
                    $msg_error = "No special trip for you";
                }
                
            $get_data  = \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price','user_car.car_name','user_car.plate', 'user_car.color')
                                     ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                     ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                     ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                     ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                     ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                     ->where([['special_trip.is_driver', '1'],['special_trip.state' , 'need'], ['special_trip.user_id' , auth()->user()->id]])->get();
                                     
                                     
            if( count( $get_data) >0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    public function accept_specialTrip(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم قبول هذة الرحله لك";
                    $msg_error  = "يوجد خطأ، حاول مره اخرى" ;
                }else{
                    $msg_data =  "this trip is accepted for you";
                    $msg_error = "error, please try again";
                }
                
            $specialTrip_id = $request->input('specialTrip_id');
            
            $accept = \App\Special_trip::where('id' , $specialTrip_id)->update(['state' => 'wait']);
            
            if( $accept == true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $messgae['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
     public function Refused_specialTrip(Request $request){
        if(auth()->User()){
            
             $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم رفض هذة الرحله لك";
                    $msg_error  = "يوجد خطأ، حاول مره اخرى" ;
                }else{
                    $msg_data =  "this trip is accepted for you";
                    $msg_error = "error, please try again";
                }
                
            $specialTrip_id = $request->input('specialTrip_id');
            
            $accept = \App\Special_trip::where('id' , $specialTrip_id)->update(['state' => 'refuse']);
            
            if( $accept == true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $messgae['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    public function accept_tripSeats(Request $request){
        if( auth()->User()){
            
            $user_id = $request->input('user_id');
            $trip_id = $request->input('trip_id');
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
        
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم قبول مكانك فى الرحلة";
                    $msg_error  = "يوجد خطأ، حاول مره اخرى" ;
                }else{
                    $msg_data =  "your seat is accepted in this trip";
                    $msg_error = "error, please try again";
                }
            
            $update = \App\Car_seat::where([['specialTrip_id' , $trip_id] , ['user_id' , $user_id]])->update([ 'state' => 'accept']);
            
            $user_token = \App\User::where('id' , $user_id)->value('firebase_token');
            
            if( $update == true){
                    
                   define( 'API_ACCESS_KEY12', 'AAAAx0JkJkg:APA91bF4mFym1nM37kLnaTqgDnUVBqbhp41Xx5GH978IoJUT9MSLh03T6DxfB98R2V5SXgeiEQ0LkB0ZGZ7cOv6m8JSaxWIu-BJOYceF6r1PFxG022AfwFLP-l_T86l4aUjyqONFZ0tt');
    
    
                        #prep the bundle
                             $msg = array
                                  (
                        		'body' 	=> "تم قبول مكانك فى الرحلة",
                        		'title'	=>  "Taxina Trip",
                                'click_action' => "4",
                                
                                  );
                        	$fields = array
                        			(
                        				'to'		=> $user_token,
                                        'data' => $mj = array(
                                                    'redirect_id' => "$trip_id",
                                                    'type' => "2",
                                                    'status' => "4",
                                                ),
                        				'notification'	=> $msg
                        			);
                        	
                        	                 
    
                        	$headers = array
                        			(
                        				'Authorization: key=' . API_ACCESS_KEY12,
                        				'Content-Type: application/json'
                        			);
                        #Send Reponse To FireBase Server	
                        		$ch = curl_init();
                        		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        		curl_setopt( $ch,CURLOPT_POST, true );
                        		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        		$result = curl_exec($ch );
                        		//echo $result;
                        		curl_close( $ch );
                        		
                    		
                    			
                        		$add_notify = new \App\Notification;
                            	
                            	$add_notify->body = "تم قبول مكانك فى الرحلة";
                            	$add_notify->title =  "Taxina Trip";
                            	$add_notify->click_action = 4;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 2;
                            	$add_notify->status = 4;
                            	$add_notify->user_id = $user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
     public function refuse_tripSeats(Request $request){
        if( auth()->User()){
            
            $user_id = $request->input('user_id');
            $trip_id = $request->input('trip_id');
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
        
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم إلغاء مكانك فى الرحلة";
                    $msg_error  = "يوجد خطأ، حاول مره اخرى" ;
                }else{
                    $msg_data =  "your seat is refused in this trip";
                    $msg_error = "error, please try again";
                }
            
            $update = \App\Car_seat::where([['specialTrip_id' , $trip_id] , ['user_id' , $user_id]])->update([ 'state' => 'refuse' , 'user_id'  => 0 , 'is_ride' => 0]);
    

            $user_token = \App\User::where('id' , $user_id)->value('firebase_token');

            if( $update == true){
                
                  
                   define( 'API_ACCESS_KEY12', 'AAAAx0JkJkg:APA91bF4mFym1nM37kLnaTqgDnUVBqbhp41Xx5GH978IoJUT9MSLh03T6DxfB98R2V5SXgeiEQ0LkB0ZGZ7cOv6m8JSaxWIu-BJOYceF6r1PFxG022AfwFLP-l_T86l4aUjyqONFZ0tt');
    
    
                        #prep the bundle
                             $msg = array
                                  (
                        		'body' 	=> "تم إلغاء مكانك فى الرحلة",
                        		'title'	=>  "Taxina Trip",
                                'click_action' => "5",
                                
                                  );
                        	$fields = array
                        			(
                        				'to'		=> $user_token,
                                        'data' => $mj = array(
                                                    'redirect_id' => "$trip_id",
                                                    'type' => "2",
                                                    'status' => "5",
                                                ),
                        				'notification'	=> $msg
                        			);
                        	
                        	                 
    
                        	$headers = array
                        			(
                        				'Authorization: key=' . API_ACCESS_KEY12,
                        				'Content-Type: application/json'
                        			);
                        #Send Reponse To FireBase Server	
                        		$ch = curl_init();
                        		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        		curl_setopt( $ch,CURLOPT_POST, true );
                        		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        		$result = curl_exec($ch );
                        		//echo $result;
                        		curl_close( $ch );
                        		
                    		
                    			
                        		$add_notify = new \App\Notification;
                            	
                            	$add_notify->body = "تم إلغاء مكانك فى الرحلة";
                            	$add_notify->title =  "Taxina Trip";
                            	$add_notify->click_action = 5;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 2;
                            	$add_notify->status = 5;
                            	$add_notify->user_id = $user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    /************************************************************ dashboard ***********************/
    
    
    
    
 public function show_allendspecialtrip(Request $request){
    if(Auth()->User()){

        $data=Special_trip::select('special_trip.id', 'fromarea.name as from_area', 'toarea.name as to_area', 'seat_count', 'special_trip.date', 'special_trip.from_time', 'special_trip.to_time', 'special_trip.price', 'special_trip.code', 'special_trip.rate', 'users.first_name','users.last_name','users.image', 'special_trip.state', 'special_trip.created_at', 'special_trip.updated_at')
                             ->join('area as fromarea','special_trip.from_areaId','=','fromarea.id')
                             ->join('area as toarea','special_trip.to_areaId','=','toarea.id')
                             ->join('users','special_trip.user_id','=','users.id')
                              ->where('special_trip.state','end')
                              ->orderBy('special_trip.id')->get();

        if(count($data)>0){
          
              $message['data']=$data;
              $message['error']=0;
               $message['message']='show all special trips';
        }else{
              $message['data']=$data;
              $message['error']=1;
               $message['message']='no data';
        }
    }else{

        $message['error'] = 2;
        $message['message'] = 'this token is not provided';
     }
    return response()->json($message);
}




public function show_needspecialtrip(Request $request){
   if(Auth()->User()){

        $data=Special_trip::select('special_trip.id', 'fromarea.name as from_area', 'toarea.name as to_area', 'seat_count', 'special_trip.date', 'special_trip.from_time', 'special_trip.to_time', 'special_trip.price', 'special_trip.code', 'special_trip.rate','user_id', 'users.first_name','users.last_name','users.image', 'special_trip.state', 'special_trip.created_at', 'special_trip.updated_at')
                          ->join('area as fromarea','special_trip.from_areaId','=','fromarea.id')
                          ->join('area as toarea','special_trip.to_areaId','=','toarea.id')
                          ->join('users','special_trip.user_id','=','users.id')
                          ->where('special_trip.state','need')
                          ->orderBy('special_trip.id' , 'DESC')->get();

        if(count($data)>0){
          
              $message['data']=$data;
              $message['error']=0;
               $message['message']='show all special trips needs';
        }else{
              $message['data']=$data;
              $message['error']=1;
               $message['message']='no data';
        }
       }else{

            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
    return response()->json($message);
}





    public function show_refusedspecialtrip(Request $request){
        if(Auth()->User()){

             $data=Special_trip::select('special_trip.id', 'fromarea.name as from_area', 'toarea.name as to_area', 'seat_count', 'special_trip.date', 'special_trip.from_time', 'special_trip.to_time', 'special_trip.price', 'special_trip.code', 'special_trip.rate', 'users.first_name','users.last_name','users.image', 'special_trip.state', 'special_trip.created_at', 'special_trip.updated_at')
                                 ->join('area as fromarea','special_trip.from_areaId','=','fromarea.id')
                                 ->join('area as toarea','special_trip.to_areaId','=','toarea.id')
                                 ->join('users','special_trip.user_id','=','users.id')
                                 ->where('special_trip.state','refuse')
                                ->orderBy('special_trip.id')->get();
    
            if(count($data)>0){
                   $message['data']=$data;
                   $message['error']=0;
                   $message['message']='show all special trips';
            }else{
                   $message['data']=$data;
                   $message['error']=1;
                   $message['message']='no data';
            }
       }else{

            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
    return response()->json($message);
      
}




public function delete_specialtrip(Request $request){
   if(Auth()->User()){
        $id=$request->input('id');

        $delete=Special_trip::where('id',$id)->delete();

        if($delete ==true){
            $message['error']=0;
            $message['message']='delete  special trips success';
        }else{
           $message['error']=1;
           $message['message']='error in delete';
        }
       }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
     return response()->json($message);
}
    
    
    
    
    
    
    
     public function new_specialtrip(Request $request){
        
        if(Auth()->User()){
            $driver_id = $request->input('driver_id');

            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
        
           $check = \App\User_car::where([['user_id', $driver_id] , ['type_id' , '2']])->first();
            
            if($check  != NULL){
                $add = new \App\Special_trip;
    
                 
                $add->from_areaId = $request->input('from_areaId');
                $add->to_areaId = $request->input('to_areaId');
                $add->seat_count = $request->input('seat_count');
                $add->date = $request->input('date');
                $add->from_time = $request->input('from_time');
                $add->to_time = $request->input('to_time');
                $add->price = $request->input('price');
                $add->user_id = $driver_id;
                $add->is_driver =1;
                $add->state = 'need';
                $add->created_at = $dateTime;
                $add->save();
    
    
                $each_user = $request->input('price') /$request->input('seat_count');
                
                $seats = array();
                
                for($x = 1; $x<= $request->input('seat_count'); $x++){
                    $seats[] = [
                        "specialTrip_id" => $add->id,
                        "driver_id" => $driver_id,
                        "seat_num" => $x,
                        "money"=> $each_user,
                        "user_id" => 0,
                        "is_ride" => 0,
                        "created_at" => $dateTime,
                    ];
                }
    
                $add_seats = \App\Car_seat::insert($seats);
    
               $code = rand(1000,9999);
        
              $update_code = \App\Special_trip::where('id', $add->id)->update(['code' => $code]);
    
    
                if( $add ==true && $add_seats == true){
                    $message['error'] = 0;
                    $message['message'] ='add success';
                }else{
                    $message['error'] = 1;
                    $message['message'] ='error in save data';
                }
            }else{
                $message['error'] = 4;
                $message['message'] ='this driver not has a bus';
            }
       }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
 }

}
