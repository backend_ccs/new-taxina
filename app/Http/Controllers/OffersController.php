<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

use carbon\carbon;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Illuminate\Support\Str;
use App\User_car;
use App\Car_seat;
use App\User_offer;

class OffersController extends Controller
{
    public $message=array();

  public function show_user_offers(Request $request){

         if( Auth()->User()){
    
                $user_id = $request->input('user_id');
    
    		    $select=User_offer::select('user_offer.id','offers.title','end_date','value','promo_code','user_offer.state','user_offer.created_at')
                    		      ->join('offers','user_offer.offer_id','=','offers.id')
                    		     ->where('user_offer.user_id',$user_id)->get();
    
    
              if(count($select)>0 ){
    
                $message['data']=$select;
                $message['error']=0;
                $message['message']='show data success';
              }else{
    
                $message['data']=$select;
                $message['error']=1;
                $message['message']='no data exist';
    
              }
	      }else{

	      	    $message['error'] = 2;
	            $message['message'] = 'this token is not provided';
	      }
	return response()->json($message);

  }


public function delete_user_offer(Request $request){

         if( Auth()->User()){

             $id = $request->input('id');
    
    		 $delete=User_offer::where('id',$id)->delete();
    
    
              if($delete ==true ){
    
                $message['error'] = 0;
                $message['message'] = 'Promo code is deleted successfuly';
              }else{
    
                $message['error'] = 1;
                $message['message'] = 'error in delete';
    
              }
	      }else{

	      	    $message['error'] = 2;
	            $message['message'] = 'this token is not provided';
	      }
	return response()->json($message);

} 
    
    
public function close_user_offer(Request $request){

         if( Auth()->User()){

             $id = $request->input('id');
    
    		 $updates = User_offer::where('id', $id)->update([ 'state' => 'close']);
    
    
              if($updates == true ){
    
                $message['error'] = 0;
                $message['message'] = 'Promo code is closed for that user successfuly';
              }else{
    
                $message['error'] = 1;
                $message['message'] = 'error in delete';
    
              }
	      }else{

	      	    $message['error'] = 2;
	            $message['message'] = 'this token is not provided';
	      }
	return response()->json($message);

} 

public function show_user_offerbyid(Request $request){

         $id=$request->input('id');
          if( Auth()->User()){
              
      

	     $select=User_offer::select('user_offer.id','offers.id as offer_id','offers.title','user_offer.state','user_offer.created_at')
		 ->join('offers','user_offer.offer_id','=','offers.id')
		->where('user_offer.id',$id)->first();


          if($select !=null ){

            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data success';
          }else{

            $message['data']=NULL;
            $message['error']=1;
            $message['message']='no data exist';

          }
		      }else{

		      	    $message['error'] = 2;
		            $message['message'] = 'this token is not provided';
		      }
	
        return response()->json($message);

 } 

 public function insert_user_offer(Request $request){
     if(Auth()->User()){

	          
	       $updated_at = carbon::now()->toDateTimeString();
           $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

           $user_id = $request->input('user_id');
	       $offer_id =$request->input('offer_id');

          
          $select=User_offer::where([['user_id',$user_id],['offer_id',$offer_id]])->first();

          if($select !=null){
              
               $message['error']=4;
               $message['message']='user has this offer';
              
          }else{
	         
	         $insert=new User_offer;
	         $insert->user_id=$user_id;
	         $insert->offer_id=$offer_id;
	         $insert->state='open';
	         $insert->created_at=$dateTime;
	         $insert->save();
	         
	     	if($insert == true){
     	 		
              $message['error']=0;
               $message['message']='insert user_offer success';
     	 	}else{
              $message['error']=1;
               $message['message']='error in insert user_offer';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  

     	 
     
            return response()->json($message);
	       
}
	
	
	/*public function update_user_offer(Request $request){
	  
	       $id=$request->input('id');  
	       $offer=$request->input('offer_id');
	       $state=$request->input('state');
 
	     if(Auth()->User()){

	          
	     $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


	      $update=User_offer::where('id',$id)->update([
             'title'=>$title,
             'image'=>$images,
             'updated_at'=>$dateTime

	        ]);
	        
	     	if($update == true){
     	 		
              $message['error']=0;
               $message['message']='update type success';
     	 	}else{
              $message['error']=1;
               $message['message']='error in update type';
     	 	}
          
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  

     	 
    
            return response()->json($message);
	       
	}*/

}
