<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\helpers\ResponseHelper as res;
use App\Models\RideModel;
// use App\resources\RideResource;
use App\Http\Resources\RideResource;

class RideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userRequestRide(Request $request)
    {


        $validation = Validator::make($request->all(), [
            "user_id" => 'required',
            "start_lat" => 'required',
            "start_long" => 'required',
            "end_lat" => 'required',
            "end_long" => 'required',
            "car_type_id" =>'required',
        ]);

        if( $validation -> fails()){
            $message['error'] = 442;
            $message['message'] = $validation->messages()->first();
            $message['data'] = null;

            return response()->json($message);
        }

        $ride = new RideModel;

        $ride->user_id = $request->input('user_id');
        $ride->start_lat = $request->input('start_lat');
        $ride->start_long = $request->input('start_long');
        $ride->start_location = $request->input('start_location');
        $ride->end_lat = $request->input('end_lat');
        $ride->end_long = $request->input('end_long');
        $ride->end_location = $request->input('end_location');
        $ride->car_type_id = $request->input('car_type_id');
        $ride->status = "pending";
        $ride->distance = $request->input('distance');
        $ride->distance = $request->input('distance');
        

        $ride->save();

        $rideResource = new RideResource($ride);
        $this->fireEventToDrivers($rideResource);

        $message['code'] = 201;
        $message['message'] = "insert success";
        $message['data']['ride'] =$rideResource ;

        return response()->json($message);

    }

    public function ridestatus(Request $request)
    {

        $validation = Validator::make($request->all(), [
            "user_id" => 'required',
            "id" => 'required',
            "status" => 'required',
        ]);

        if( $validation -> fails()){
            $message['error'] = 442;
            $message['message'] = $validation->messages()->first();
            $message['data'] = null;

            return response()->json($message);
        }



        $ride = RideModel::findOrFail($request->input('id'));

        $ride->user_id = $request->input('user_id');
        $ride->status = $request->input('status');

        if ($ride->save()) {
            $message['data']['ride'] =new RideResource($ride);
            $message['code'] = 200;
            $message['message'] = "update success";
        }

        
        
        return $message;

    }


    public function rideUserRate(Request $request)
    {

        $validation = Validator::make($request->all(), [
            "user_id" => 'required',
            "id" => 'required',
            "user_rate" => 'required',
        ]);

        if( $validation -> fails()){
            $message['error'] = 442;
            $message['message'] = $validation->messages()->first();
            $message['data'] = null;

            return response()->json($message);
        }



        $ride = RideModel::findOrFail($request->input('id'));

        $ride->user_id = $request->input('user_id');
        $ride->user_rate = $request->input('user_rate');

        if ($ride->save()) {
            $message['data']['ride'] =new RideResource($ride);
            $message['code'] = 200;
            $message['message'] = "update success";
        }

        
        
        return $message;

    }
    public function rideDriverRate(Request $request)
    {

        $validation = Validator::make($request->all(), [
            "driver_id" => 'required',
            "id" => 'required',
            "driver_rate" => 'required',
        ]);

        if( $validation -> fails()){
            $message['error'] = 442;
            $message['message'] = $validation->messages()->first();
            $message['data'] = null;

            return response()->json($message);
        }



        $ride = RideModel::findOrFail($request->input('id'));

        $ride->user_id = $request->input('driver_id');
        $ride->driver_rate = $request->input('driver_rate');

        if ($ride->save()) {
            $message['data']['ride'] =new RideResource($ride);
            $message['code'] = 200;
            $message['message'] = "update success";
        }

        
        
        return $message;

    }


    public function userRides($id)
    {
        //

        $ride = RideModel::where('user_id',  $id)->get();

        // return RideResource::collection($ride);
        
        $message['data']['rides'] =RideResource::collection($ride);
        $message['code'] = 200;
        $message['message'] = "update success";
        return $message;
    }

    public function driverRides($id)
    {
        //

        $ride = RideModel::where('driver_id',  $id)->get();

        // return RideResource::collection($ride);
        
        $message['data']['rides'] =RideResource::collection($ride);
        $message['code'] = 200;
        $message['message'] = "update success";
        return $message;
        
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function single($id)
    {
        //
        $ride = RideModel::findOrFail($id);

        // return RideResource::collection($ride);
        
        $message['data']['ride'] =new RideResource($ride);
        $message['code'] = 200;
        $message['message'] = "update success";
        return $message;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }











    // real time events
    public function fireEventToDrivers(RideResource $rideResource){

    }
}
