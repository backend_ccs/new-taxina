<?php
namespace App\Http\Controllers\Auth;

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Carbon\Carbon;
use App\User_car;

class userController extends Controller
{
  
    public $message = array();
    
    
    public function user_registration(Request $request){


        $created_at = carbon::now()->toDateTimeString();
        $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

        $email = $request->input('email');
        $phone = $request->input('phone');
        $gender = $request->input('gender');
        
        if( $gender == '1'){
            $gend = "female";
        }else{
            $gend = "male";
        }
        
        
        // if (\App\User::where($email , '=', Input::get('email'))->exists()){
        //     $message['data'] = NULL;
        //     $message['error'] = 3;
        //     $message['message'] = "Email is already exists";
        // }elseif(\App\User::where($phone , '=', Input::get('phone'))->exists()){
        //     $message['data'] = NULL;
        //     $message['error'] = 4;
        //     $message['message'] = "Phone is already exists";
        
        
        $check_email = \App\User::where('email' , $email)->value('id');
        $check_phone = \App\User::where('phone' , $phone)->value('id');

        if( $check_email != NULL ){
            $message['data'] = NULL;
            $message['error'] = 3;
            $message['message'] = "Email is already exists";
        }elseif( $check_phone != NULL){
            $message['data'] = NULL;
            $message['error'] = 4;
            $message['message'] = "Phone is already exists";
        }else{

            $add = new \App\User;

            $add->first_name = $request->input('first_name');
            $add->last_name  = $request->input('last_name');
            $add->email      = $email;
            $add->phone      = $phone;
            $add->password   = bcrypt($request->input('password'));
            $add->gender     = $gend;
            $add->image     = "http://taxiApi.codecaique.com/uploads/users/img.png";
            $add->Latitude = $request->input('Latitude');
            $add->Longitude = $request->input('Longitude');
            $add->role       = 2;
            $add->is_online  = 0;
            $add->is_driver  = 0;
            $add->is_busy    = 0;
            $add->wallet    = 0;
            $add->firebase_token = $request->input('firebase_token');
            $add->created_at = $dateTime;
            $add->save();

            $token = JWTAuth::fromUser($add);
            $data = \App\User::where('id',$add->id)->first();
            $data['token'] = $token;
                
            $add_setting = new \App\Setting;
            
            $add_setting->language = "en";
            $add_setting->user_id = $add->id;
            $add_setting->save();
                

            if( $add == true){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = "you are registers successfully";
            }
        }
        return Response::json($message);

    }
    
    
     public function add_users(Request $request){


        $created_at = carbon::now()->toDateTimeString();
        $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

        $email = $request->input('email');
        $phone = $request->input('phone');
        $gender = $request->input('gender');
        
        if( $gender == '1'){
            $gend = "female";
        }else{
            $gend = "male";
        }
        
        $image= $request->file('image');
        
            if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/users';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/users/".$savedFileName;
            }else{
                $images = "http://taxiApi.codecaique.com/uploads/users/img.png";
            }
        $check_email = \App\User::where('email' , $email)->value('id');

        $check_phone = \App\User::where('phone' , $phone)->value('id');

        if( $check_email != NULL ){
            $message['data'] = NULL;
            $message['error'] = 3;
            $message['message'] = "Email is already exists";
        }elseif( $check_phone != NULL){
            $message['data'] = NULL;
            $message['error'] = 4;
            $message['message'] = "Phone is already exists";
        }else{

            $add = new \App\User;

            $add->first_name = $request->input('first_name');
            $add->last_name  = $request->input('last_name');
            $add->email      = $email;
            $add->phone      = $phone;
            $add->password   = bcrypt($request->input('password'));
            $add->gender     = $gend;
            $add->image      = $images;
            $add->Latitude = 0;
            $add->Longitude = 0;
            $add->role       = 2;
            $add->is_online  = 0;
            $add->is_driver  = 0;
            $add->is_busy    = 0;
            $add->wallet    = 0;
            $add->firebase_token = "";
            $add->created_at = $dateTime;
            $add->save();
                
            $add_setting = new \App\Setting;
            
            $add_setting->language = "en";
            $add_setting->user_id = $add->id;
            $add_setting->save();
                

            if( $add == true){
                $message['error'] = 0;
                $message['message'] = "you are registers successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "error, please try again";
            }
        }
        return Response::json($message);

    }


    public function user_login(Request $request){
   
          
        try{
             $msg_data ="";
             $msg_error ="";
             
            if ( $token = JWTAuth::attempt( ["email" => $request->input('email') , "password" => $request->input('password') , "role" => '2']) ) {
                
                $update = \App\User::where('id' , auth()->User()->id)->update(['firebase_token' => $request->input('firebase_token')]);
                
                $data = \App\User::where('id',Auth()->User()->id)->first();
                $data['token'] = $token;

                $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "مرحباّ، تم الدخول بنجاح";
                    $msg_error = "عفوا بيانات الدخول خاطئة من فضلك حاول مرة اخرى";
                }else{
                    $msg_data = "Welcome, you are logged in";
                    $msg_error = "Email or Password is wrong!!";
                }
                
                $message['data'] =$data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
                return response()->json( $message,200);

            }else{
                $message['data'] = NULL;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

            
        }catch(JWTException $e){

            return response()->json( ['error'=> 'could not create token'],500);
        }
   
   
           return response()->json( $message);
    }



    public function social_login(Request $request){
        try{
             $msg_data ="";
             $msg_error ="";
            
             $email = $request->input('email');
             $user = User::where('email', '=', $email)->first();

            if ( $token = JWTAuth::fromUser($user) ) {
                
                //$update = \App\User::where('id' , auth()->User()->id)->update(['firebase_token' => $request->input('firebase_token')]);
                
                $data = $user; //\App\User::where('id',auth()->User()->id)->first();
                $data['token'] = $token;

                $check_setting = \App\Setting::where('user_id' , $user->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "مرحباّ، تم الدخول بنجاح";
                    $msg_error  = "عفوا بيانات الدخول خاطئة من فضلك حاول مرة اخرى";
                }else{
                    $msg_data = "Welcome, you are logged in";
                    $msg_error = "Email is wrong!!";
                }
                
                $message['data'] =$data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
                return response()->json( $message,200);

            }else{
                $message['data'] = NULL;
                $message['error'] = 1;
                $message['message'] = "error please try again";
            }

            
        }catch(JWTException $e){

            return response()->json( ['error'=> 'could not create token'],500);
        }
   
   
           return response()->json( $message);
    }

    // public function getUser(Request $request){

    //     try{
    //         if(auth()->user()){
    //             return response()->json( [
    //                 "data" => auth()->user(),
    //                 "success" => 'true',
    //                 "message" => " this is the data of the user",
    //             ]);
    //         }else{
    //             return response()->json( [
    //                 "data" => auth()->user(),
    //                 "success" => 'false',
    //                 "message" => "Token is not provided",
    //             ]);
    //         }

    //     }catch(JWTException $e){
   
    //         return response()->json( ['error'=> 'could not create token'],500);
    //       }
    // }



    public function show_user_ByID(Request $request){

        if(auth()->user()){
             $msg_data ="";
             $msg_error ="";
            $user_id = $request->input('user_id');

            if( $request->has('user_id') && $user_id != NULL){
            
                $data = User::select('id','first_name','last_name','email','phone','gender','image','Latitude','Longitude','rate','role','is_online','is_driver','is_busy','firebase_token')->where('id', $user_id)->first();


                 $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "بيانات المستخدم";
                    $msg_error  = "لا يوجد مستخدم";
                }else{
                    $msg_data = "user data";
                    $msg_error = "there is no user";
                }
                
                if( is_null($data)){
                
                    $message["data"] = NULL;
                    $message["error"] = 1;
                    $message["message"] = $msg_error;
                }else{
                    $message["data"] = $data;
                    $message["error"] = 0;
                    $message["message"] = $msg_data;
                }
            }else{
                $message["data"] = auth()->user();
                $message["error"] = 0;
                $message["message"] = $msg_data;

            }

           
        }else{
                $message["data"] = auth()->user();
                $message["error"] = 2;
                $message["message"] = "Token is not provided";
        }
        return response()->json( $message);

    }



 public function show_userByID(Request $request){

        if(auth()->user()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";
            $user_id = $request->input('user_id');

            if( $request->has('user_id') && $user_id != NULL){
            
                $data = User::select('users.id','users.first_name','users.last_name','users.email','users.phone','users.gender','users.image','users.Latitude','users.Longitude','users.rate','users.role','users.is_online','users.is_driver','users.is_busy','users.firebase_token','user_country.country_id')
                            ->join('user_country', 'users.id', '=' , 'user_country.user_id' )
                            ->where('users.id', $user_id)->first();

                if( $data->gender == 'male'){
                    $gend = 0;
                }else{
                    $gend = 1;
                }

                $data['gender_id'] = $gend;
          
                if( is_null($data)){
                
                    $message["data"] = NULL;
                    $message["error"] = 1;
                    $message["message"] = $msg_error;
                }else{
                    $message["data"] = $data;
                    $message["error"] = 0;
                    $message["message"] = $msg_data;
                }
            }else{
                $message["data"] = auth()->user();
                $message["error"] = 0;
                $message["message"] = $msg_data;

            }

           
        }else{
                $message["data"] = auth()->user();
                $message["error"] = 2;
                $message["message"] = "Token is not provided";
        }
        return response()->json( $message);

    }



    public function update_profile(Request $request){

        if( Auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";
            
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
            
            $image = $request->file('image');

            $check_image = \App\User::select('first_name','last_name','image')->where('id' , Auth()->User()->id)->first();

            if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/users';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/users/".$savedFileName;
            }else{
                if( $check_image != NULL){
                    $images = $check_image->image;
                }else{
                    $images = NULL;
                }
            }
            
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            
            if( $request->has('first_name') && $first_name != NULL){
                $firstname = $first_name;
            }else{
                $firstname = $check_image->first_name;
            }


            if( $request->has('last_name') && $last_name != NULL){
                $lastname = $last_name;
            }else{
                $lastname = $check_image->last_name;
            }

            $update_data = \App\User::where('id' , Auth()->User()->id)
                                    ->update([
                                        "first_name" => $firstname,
                                        "last_name" => $lastname ,
                                        "email" => $request->input('email'),
                                        "phone" => $request->input('phone'),
                                        "image" => $images,
                                        "updated_at" => $dateTime
                                    ]);
            
           
                                    
             $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "نم تعديل البيانات بنجاح";
                    $msg_error  ="يوجد خطأ حاول مره اخرى";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "Profile is updated successfully";
                    $msg_error = "there is an error, please try again";
                    $msg_token = "Token is not provided";   
                }

            if( $update_data == true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";


        }
        return response()->json($message);

        
    }




  public function update_usersData(Request $request){

        if( Auth()->User()){
             
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
            
            $user_id = $request->input('user_id');
            
            $image = $request->file('image');

            $check_image = \App\User::select('first_name','last_name','image')->where('id' , $user_id)->first();

            if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/users';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/users/".$savedFileName;
            }else{
                if( $check_image != NULL){
                    $images = $check_image->image;
                }else{
                    $images = NULL;
                }
            }
            
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            
            if( $request->has('first_name') && $first_name != NULL){
                $firstname = $first_name;
            }else{
                $firstname = $check_image->first_name;
            }


            if( $request->has('last_name') && $last_name != NULL){
                $lastname = $last_name;
            }else{
                $lastname = $check_image->last_name;
            }

            $gender = $request->input('gender');
                    
            if( $gender == '1'){
                $gend = "female";
            }else{
                $gend = "male";
            }
            $update_data = \App\User::where('id' , $user_id)
                                    ->update([
                                        "first_name" => $firstname,
                                        "last_name" => $lastname ,
                                        "email" => $request->input('email'),
                                        "phone" => $request->input('phone'),
                                        "gender" => $gend,
                                        "image" => $images,
                                        "updated_at" => $dateTime
                                    ]);
                                    
             $country_id = $request->input('country_id');
            
            
            if($request->has('country_id')){
                
                $updateCountry = \App\User_country::where('user_id' , $user_id)->update(['country_id' => $country_id]);
            } 
            
            if( $update_data == true){
                $message['error'] = 0;
                $message['message'] = "Profile is updated successfully";;
            }else{
                $message['error'] = 1;
                $message['message'] = "error, please try again";
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";


        }
        return response()->json($message);

        
    }

    public function add_favorite_place(Request $request){

        if( Auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";

            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
            
            $add = new \App\Favorite_place;

            $add->name = $request->input('name');
            $add->Latitude = $request->input('Latitude');
            $add->Longitude = $request->input('Longitude');
            $add->address = $request->input('address');
            $add->user_id = Auth()->User()->id;
            $add->created_at = $dateTime;
            $add->save();
            
                                    
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data ="نم أضافه هذا المكان الى المفضلة"; 
                    $msg_error  = "يوجد خطأ حاول مره اخرى";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "new place add to favorite";
                    $msg_error = "there is an error, please try again";
                    $msg_token = "Token is not provided";   
                }

            if( $add == true ){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";


        }
        return response()->json($message);

    }


    public function show_userFavorite_place(Request $request){
        if( Auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";
            $get_data = \App\Favorite_place::select('id', 'name','Latitude','Longitude' , 'address')
                                            ->where('user_id' , auth()->user()->id)->get();
                                            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "الاماكن المفضلة";
                    $msg_error  =  "لايوجد اماكن مفضلة";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "this is all favorite places";
                    $msg_error =  "No favorite place for that user";
                    $msg_token = "Token is not provided";   
                }

            if( count($get_data ) >0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            } 

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);

    }
    
    
    public function delete_favorite(Request $request){
        if( Auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";
             $id = $request->input('id');
            $get_data = \App\Favorite_place::where('id' , $id)->delete();
                                            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم مسح هذا المكان";
                    $msg_error  =  "خطأ حاول مره اخرى ";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = " place is deleted";
                    $msg_error =  "error, please try again";
                    $msg_token = "Token is not provided";   
                }

            if( $get_data== true ){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            } 

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);

    }
    
    
    /*************************************** driver *******************************/
    
    
    public function Driver_registration(Request $request){


        $created_at = carbon::now()->toDateTimeString();
        $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

        $email = $request->input('email');
        $phone = $request->input('phone');
        $gender = $request->input('gender');
        
        if( $gender == '1'){
            $gend = "female";
        }else{
            $gend = "male";
        }
        $check_email = \App\User::where('email' , $email)->value('id');

        $check_phone = \App\User::where('phone' , $phone)->value('id');

        if( $check_email != NULL ){
            $message['data'] = NULL;
            $message['error'] = 3;
            $message['message'] = "Email is already exists";
        }elseif( $check_phone != NULL){
            $message['data'] = NULL;
            $message['error'] = 4;
            $message['message'] = "Phone is already exists";
        }else{

            $add = new \App\User;

            $add->first_name = $request->input('first_name');
            $add->last_name  = $request->input('last_name');
            $add->email      = $email;
            $add->phone      = $phone;
            $add->password   = bcrypt($request->input('password'));
            $add->gender     = $gend;
            $add->Latitude = $request->input('Latitude');
            $add->Longitude = $request->input('Longitude');
            $add->role       = 3;
            $add->is_online  = 0;
            $add->is_driver  = 1;
            $add->is_busy    = 0;     
            $add->wallet     = 0;
            $add->firebase_token = $request->input('firebase_token');
            $add->created_at = $dateTime;
            $add->save();

            $token = JWTAuth::fromUser($add);
            $data = \App\User::where('id',$add->id)->first();
            $data['token'] = $token;
                
            $add_setting = new \App\Setting;
            
            $add_setting->language = "en";
            $add_setting->user_id = $add->id;
            $add_setting->save();
            
            
            $add_userCountry = new \App\User_country;
            $add_userCountry->user_id = $add->id;
            $add_userCountry->country_id = $request->input('country_id');
            $add_userCountry->save();
            
            if( $add == true){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = "you are registers successfully";
            }
        }
        return Response::json($message);

    }
    
     public function Driver_login(Request $request){
     
        try{
             $msg_data ="";
             $msg_error ="";
            if ( $token = JWTAuth::attempt( ["email" => $request->input('email') , "password" => $request->input('password') , "role" => '3']) ) {
                
                $update = \App\User::where('id' , auth()->User()->id)->update(['firebase_token' => $request->input('firebase_token') , 'is_online' => '1']);
                
                $data = \App\User::where('id',Auth()->User()->id)->first();
                $data['token'] = $token;

                $get_userCar = \App\User_car::where('user_id' , auth()->User()->id)->value('type_id');
                
                $data['car_type'] = $get_userCar;
                $data['country_id'] = \App\User_country::where('user_id' , auth()->User()->id)->value('country_id');
                $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "مرحباّ، تم الدخول بنجاح";
                    $msg_error  =  "البريد الإلكتروني أو كلمة المرور خاطئة !!";
                }else{
                    $msg_data = "Welcome, you are logged in";
                    $msg_error =  "Email or Password is wrong!!";
                }
                
                $message['data'] =$data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
                
                return response()->json( $message,200);

            }else{
                $message['data'] = NULL;
                $message['error'] = 1;
                $message['message'] = "عفوا بيانات الدخول خاطئة من فضلك حاول مرة اخرى";
            }

            
        }catch(JWTException $e){

            return response()->json( ['error'=> 'could not create token'],500);
        }
   
   
           return response()->json( $message);
    }
    
    
    public function online_offline (Request $request){
        if(auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";
            $check = \App\User::where('id' , auth()->User()->id)->value('is_online');
            
            if( $check == '1') {     // 1=> online    0=> offline
                $x = 0;
                $val_en = "offline";
                $val_ar = "غير متاح";
            }else{
                $x = 1;
                $val_en = "online";
                $val_ar = "متاح";
            }
            
            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "انت الان ".$val_ar;
                $msg_error  ="يوجد خطأ حاول مره اخرى";
                $msg_token = " من فضلك سجل الدخول";
            }else{
                $msg_data = "you are ".$val_en." now";
                $msg_error = "there is an error, please try again";
                $msg_token = "Token is not provided";   
            }
            
                
            $update = \App\User::where('id' , auth()->User()->id)->update(['is_online' => $x]);
            
            if( $update == true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
    }


    public function change_language(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";
            $lang = $request->input('lang');
            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "تم تغير اللغه";
                $msg_error  ="يوجد خطأ حاول مره اخرى";
            }else{
                $msg_data = "language changed successfully";
                $msg_error = "there is an error, please try again";
            }
            
           
            
            $update = \App\Setting::where('user_id' , auth()->User()->id)->update(['language' => $lang]);
            
            if( $update == true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
                
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";
            
        }
        return response()->json($message);

    }
    
    
    public function user_wallet(Request $request){
        if(auth()->user()){
            
            $data = array();
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "جميع البيانات المحفظه";
                $msg_error  ="لا يوجد بيانات";
            }else{
                $msg_data = "driver wallet data";
                $msg_error = "No data";
            }
            
            $check_userCar = \App\User_car::where('user_id' , auth()->User()->id)->value('type_id');
            
            if( $check_userCar == '1'){
            
                $wallet = \App\User::select('wallet' , 'target')->where('id' , auth()->user()->id)->first();
                
                
                $total_earned = DB::select("SELECT count(id) as count ,sum(price) as earned FROM `trip` WHERE MONTH(CURRENT_DATE()) and driver_id = ?",[auth()->user()->id]);
    
                $data['target'] = $wallet->target;
                $data['driver_wallet'] = $wallet->wallet;
                $data['total_earned']  = $total_earned[0]->earned;
                $data['trip_count'] = $total_earned[0]->count;
            }else{
                $wallet = \App\User::select('wallet' , 'target')->where('id' , auth()->user()->id)->first();
                
                
                $total_earned = DB::select("SELECT count(id) as count ,sum(price) as earned FROM `special_trip` WHERE MONTH(CURRENT_DATE()) and user_id = ? and is_driver = '1'",[auth()->user()->id]);
                
                $data['target'] = $wallet->target;
                $data['driver_wallet'] = $wallet->wallet;
                $data['total_earned']  = $total_earned[0]->earned;
                $data['trip_count'] = $total_earned[0]->count;
                
            }
                
            if( $data != NULL){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
            
        }
        return response()->json($message);

    }
    
    public function change_passward(Request $request){
        
        if(auth()->User()){
            
            $password = bcrypt($request->input('password'));
            
            $update_pass = \App\User::where('id' , auth()->User()->id)->update(['password' => $password]);
            
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "تم تغير كلمه السر";
                $msg_error  = " يوجد خطأ، حاول مره اخرى ";
            }else{
                $msg_data = "Password changed successfully";
                $msg_error = " error, please try again";
            }
            
            if( $update_pass ==  true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
            
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
            
        }
        return response()->json($message);
    }


    public function update_location(Request $request){
        if(auth()->User()){
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "تم التغير بنجاح";
                $msg_error  = " يوجد خطأ، حاول مره اخرى ";
            }else{
                $msg_data = "locatioln changed successfully";
                $msg_error = " error, please try again";
            }
            
            $latitude = $request->input('latitude');
            $longitude = $request->input('longitude');
            
            $update = \App\User::where('id' , auth()->User()->id)->update(['Latitude' => $latitude , 'Longitude' => $longitude ]);
            
            
            if( $update == true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
            
        }
        return response()->json($message);
    }
    
    public function my_notification(Request $request){
        if( auth()->User()){
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "جميع الاشعارات";
                $msg_error = " لا يوجد أشعارات";
            }else{
                $msg_data = "all Notification";
                $msg_error = " No Notification";
            }
            
            $get_data = \App\Notification::where('user_id' , auth()->User()->id)->orderBy('created_at' , 'DESC')->get();
            
            if( count($get_data)>0){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
            
        }
        return response()->json($message);
    }
    
    
    
    public function show_driverCar(Request $request){
    	if(Auth()->User()){

            $user_id = $request->input('user_id');

	        $data = \App\User_car::select('user_car.id','user_car.carCat_id','user_car.car_name', 'user_car.plate', 'user_car.color', 'user_car.brand', 'user_car.year', 'user_car.driver_license', 
	                                'user_car.status','user_car.type_id','car_type.title as car_type','car_category.name as car_category', 'user_car.baka_id','bakat.name as baka_name','user_car.created_at', 'user_car.updated_at')

                                 ->leftjoin('car_type','user_car.type_id','=','car_type.id')
                                 ->leftjoin('car_category','user_car.carCat_id','=','car_category.id')
                                 ->leftjoin('users','user_car.user_id','=','users.id')
                                 ->leftjoin('bakat','user_car.baka_id','=','bakat.id')
                                 ->where('user_car.user_id',$user_id)->first();

	     	if( $data != NULL){
     	 	   $message['data']=$data;
               $message['error']=0;
               $message['message']='show all need user\'s cars';
     	 	}else{
               $message['data']=$data;
               $message['error']=1;
               $message['message']='no data';
     	 	}
     	 }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
     return response()->json($message);
    }
   
   
   public function accept_Usercar(Request $request){
	 if(Auth()->User()){
      
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

          $car_id = $request->input('car_id');

	       $accept = \App\User_car::where('id',$car_id)->update([
                                                   'status'  => 'accept',
                                                   'baka_id' => $request->input('baka_id'),
                                                   'updated_at'=>$dateTime
                                    	          ]);

	     	if( $accept ==true){
     	 		$message['error']=0;
                $message['message']='accept user_car success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in accept car';
     	 	}
        }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
        }
        return response()->json($message);
   } 
   
   
    public function refused_Usercar(Request $request){
	 if(Auth()->User()){
      
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

          $car_id = $request->input('car_id');

	       $accept = \App\User_car::where('id',$car_id)->update([
                                                   'status'  => 'refuse',
                                                   'updated_at'=>$dateTime
                                    	          ]);

	     	if( $accept ==true){
     	 		$message['error']=0;
                $message['message']='user_car is refused success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in accept car';
     	 	}
        }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
        }
        return response()->json($message);
   } 
   
   
   
   
  public function add_driver(Request $request){
    if( Auth()->User()){

        $created_at = carbon::now()->toDateTimeString();
        $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
    
        $image = $request->file('image');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $gender = $request->input('gender');
        
        if( $gender == '1'){
            $gend = "female";
        }else{
            $gend = "male";
        }
    
          if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/users';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/users/".$savedFileName;
            }else{
               
                    $images = NULL;
                
            }

        if($request->has('email')){
        $check_email = User::where('email' , $email)->value('id');
         }else{

          $check_email=array();
         }
       if($request->has('phone')){
        $check_phone = User::where('phone' , $phone)->value('id');
      }else{

        $check_phone=array();
      }

        if( $check_email != NULL ){
            $message['data'] = NULL;
            $message['error'] = 3;
            $message['message'] = "Email is already exists";
        }elseif( $check_phone != NULL){
            $message['data'] = NULL;
            $message['error'] = 4;
            $message['message'] = "Phone is already exists";
        }else{

            $add = new \App\User;

            $add->first_name = $request->input('first_name');
            $add->last_name  = $request->input('last_name');
            $add->email      = $email;
            $add->phone      = $phone;
            $add->password   = bcrypt($request->input('password'));
            $add->gender     = $gend;
            $add->image=$images;
            $add->role       = 3;
            $add->is_online  = 0;
            $add->is_driver  = 1;
            $add->is_busy    = 0;     
            $add->wallet     = 0;
            $add->target     = $request->input('target');         
            $add->created_at = $dateTime;
            $add->save();

            $add_setting = new \App\Setting;
            
            $add_setting->language = "en";
            $add_setting->user_id = $add->id;
            $add_setting->save();
            
            $add_userCountry = new \App\User_country;
            $add_userCountry->user_id = $add->id;
            $add_userCountry->country_id = $request->input('country_id');
            $add_userCountry->save();
            
            if( $add == true){
                $message['data'] = $add->id;
                $message['error'] = 0;
                $message['message'] = "you added driver successfully";
            }else{
                $message['data'] = $add->id;
                $message['error'] = 1;
                $message['message'] = "error in  added driver";
            }
        }
     }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
      }
    return Response::json($message);

    }
    
    
    
     public function add_drivercar(Request $request){

        if(auth()->user()){

             $user_id = $request->input('driver_id');
            
            
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
       
       
            $check = \App\User_car::where('user_id',$user_id)->value('id');
            
            if( $check != NULL){
                $message['error'] =4;
                $message['message'] ='this user has a car';
                
            }else{
                $add =  new User_car;
                
                $add->type_id = $request->input('type_id');
                $add->carCat_id = $request->input('carCat_id');
                $add->car_name = $request->input('car_name');
                $add->plate = $request->input('plate');
                $add->color = $request->input('color');
                $add->brand = $request->input('brand');
                $add->year = $request->input('year');
                $add->driver_license = $request->input('license_plate');
                $add->status = "accept";
                $add->baka_id = $request->input('baka_id');
                $add->user_id = $user_id;
                $add->created_at = $dateTime;
                $add->save();
                
                
                if( $add == true){
                    $message['error'] = 0;
                    $message['message'] ='add success';
                }else{
                    $message['error'] = 1;
                    $message['message'] ='error in save data';
                }
            }            
        }else{
            $message['error'] = 2;
            $message['message'] ='this  token is not provided';

        }
        return response()->json($message);
        
    }
    
    
    public function update_drivercar(Request $request){
        if(auth()->user()){

            $id=$request->input('id');
           
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
       
            $update=User_car::where('id',$id)->update([

                                        'type_id'=>$request->input('type_id'),
                                        'carCat_id'=>$request->input('carCat_id'),
                                        'car_name'=>$request->input('car_name'),
                                        'plate'=>$request->input('plate'),
                                        'color'=> $request->input('color'),
                                        'brand'=>$request->input('model'),
                                        'year'=>$request->input('year'),
                                        'driver_license'=>$request->input('license_plate'),
                                        'baka_id' => $request->input('baka_id'),
                                        'updated_at'=>$dateTime

                                         ]);
                
                
                
                
                if( $update == true){
                    $message['error'] = 0;
                    $message['message'] ='update success';
                }else{
                    $message['error'] = 1;
                    $message['message'] ='error in update data';
                }
                        
        }else{
            $message['error'] = 2;
            $message['message'] ='this  token is not provided';

        }
        return response()->json($message);
        
    }
    
    public function update_userTarget(Request $request){
        if(Auth()->User()){
         
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

            $id=$request->input('user_id');   

               $update=User::where('id',$id)->update([
                                              'target'=>$request->input('target'),
                                              'updated_at'=>$dateTime
                                
                                           ]);
                                           
               
            if($update == true){
                   $message['error']=0;
                   $message['message']='update target success';
            }else{
                   $message['error']=1;
                   $message['message']='error in updtea target';
            }
              
        }else{

            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
         
  }
  
  
    public function show_driver_bus(Request $request){
        if(auth()->User()){
    
            $get_data = \App\User_car::select('users.id', 'users.first_name', 'users.last_name', 'users.phone', 'users.gender', 'users.image', 'users.rate', 'users.is_online', 'users.is_driver', 'users.is_busy' , 'user_country.country_id')
                                     ->join('users' , 'user_car.user_id' ,'=', 'users.id')   
                                     ->join('user_country', 'user_car.user_id', '=' , 'user_country.user_id' ) 
                                     ->WHERE('user_car.type_id' , '2')->get();
                                     
         
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['message'] = "all driver bus";
            }else{
                $message['data'] = $get_data;
                $message['message'] = "No driver bus";
            }


        }else{

            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
    }
    
    
    public function forget_password(Request $request){
         
         $user_email = $request->input('email');
         
         $check_email = \App\User::where('email' , $user_email)->value('id');
         
         if( $check_email != NULL){
             $uniqid = uniqid();

             $rand_start = rand(1,5);
            
             $generate_code = substr($uniqid,$rand_start,6);
             
             $update_code_pass = \App\User::where('email', $user_email)->update(['code' => $generate_code]);
             
             
             
                $to = $user_email;
                $subject = " Taxina App";
                
                $msg = '
                <html>
                <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Taxina Password</title>
                </head>
                <body>
                 <h2>Taxina Reset Password</h2>
                
                 <p style="display:rlt;">Taxina App has recieved a request to reset the password for your account. If you did not request to reset your password, please ignore this email. </p>
                 <h4 style="text-align:center;"> Confirmation code : <br>'.$generate_code.'</h4>
                </body>
                </html>
                ';
                
                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                
                // More headers
                $headers .= 'From: codecaique@gmail.com' . "\r\n";  //sanher57@gmail.com

                mail($to,$subject,$msg,$headers);
              
              $message['code'] = $generate_code;
              $message['error'] = 0;
              $message['message'] = "the code is send to ur email successfully";
         }else{
             $message['code'] = NULL;
             $message['error'] = 1;
             $message['message'] = "email is not exist";
         }
         return response()->json($message);
     }
     
     public function Reset_password(Request $request){
     
         $code = $request->input('code');
         
         $password = bcrypt($request->input('password'));
         
         $check_code = \App\User::where([['code', $code]])->first();
         
         if( $check_code != NULL ){
             $update_pass = \App\User::where([['code', $code]])->update(['password' =>$password]);
             
             $message['error'] = 0;
             $message['message'] = "the password is updated successfully";
         }else{
             $message['error'] = 1;
             $message['message'] = "there is an error, please try again";
         }
         
         return response()->json($message);
     }
     
    
     public function privacy_policy(Request $request){
     


         $data = \App\Policy::select('id', 'title', 'content')->get();
         
         if( count($data) >0 ){
             $message['data'] = $data;    
             $message['error'] = 0;
             $message['message'] = "privacy policy data";
         }else{
             $message['data'] = NULL;
             $message['error'] = 1;
             $message['message'] = "there is an error, please try again";
         }
         
         return response()->json($message);
     }  
     
     
    public function delete_driver(Request $request){
	 if(Auth()->User()){
      
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

           $user_id = $request->input('user_id');

           $deletuser = \App\User::where('id' , $user_id)->delete();
	       
	       $deleteCar = \App\User_car::where('user_id',$user_id)->delete();

	     	if( $deletuser == true){
     	 		$message['error']=0;
                $message['message'] = 'Driver is deleted successfully';
     	 	}else{
               $message['error']=1;
               $message['message']='error in accept car';
     	 	}
        }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
        }
        return response()->json($message);
   } 
   
   
   public function check_phone(Request $request){
         
       $phone = $request->input('phone');

       $checkPhone = \App\User::where('phone' , $phone)->first();
       

     	if( $checkPhone != NULL){
 	 		$message['error']=0;
            $message['message'] = 'phone is already exist';
 	 	}else{
           $message['error']=1;
           $message['message'] = 'go to the next step';
 	 	}
      
        return response()->json($message);
   } 
   
   
   public function delete_phone(Request $request){
       
       $phone = $request->input('phone');
       
       $delete_user = \App\User::where('phone' , $phone)->delete();
       
       if($delete_user ==  true){
           $message['error'] = 0;
           $message['message'] = "this user is deleted successfully";
       }else{
           $message['error'] = 1;
           $message['message'] = "error, phone is error";
       }
       return response()->json($message);
   }
   
   
     
}
