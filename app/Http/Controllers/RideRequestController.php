<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\RideRequestModel;
use App\Http\Resources\RideRequestResource;

use Validator;
// use App\resources\RideResource;
use App\Http\Resources\RideResource;

class RideRequestController extends Controller
{

    public function showRideRequest($id){

        $rideRequest = RideRequestModel::where('ride_id',  $id)->get();

        // return RideResource::collection($ride);
        
        $message['data']['RideRequests'] =RideResource::collection($rideRequest);
        $message['code'] = 200;
        $message['message'] = "update success";

        return $message;
    }

    public function singleRideRequest($id){

        $rideRequest = RideRequestModel::findOrFail($id);

        // return RideResource::collection($ride);
        
        $message['data']['RideRequest'] =new RideResource($ride);
        $message['code'] = 200;
        $message['message'] = "update success";

        return $message;
    }

    public function createRideRequest(Request $request){

        $rideRequest = new RideRequestModel;

        $rideRequest->price = $request->input('price');
        $rideRequest->status = $request->input('status');
        $rideRequest->user_id = $request->input('user_id');
        $rideRequest->driver_id = $request->input('driver_id');
        $rideRequest->user_comment = $request->input('user_comment');
        $rideRequest->driver_comment = $request->input('driver_comment');
        $rideRequest->ride_id = $request->input('ride_id');
        // return RideResource::collection($ride);
        if ($rideRequest->save()) {
            $message['data']['RideRequest'] =new RideRequestResource ($rideRequest);
            $message['code'] = 200;
            $message['message'] = "update success";
        }

        return $message;
    }

    public function statusUserRideRequest(Request $request){

        $rideRequest = RideRequestModel::find($request->input('id'));

        if($rideRequest == null ){
            $message['data']['RideRequest'] =null;
            $message['code'] = 404;
            $message['message'] = "not found";

            return $message;
        }

        $rideRequest->status = $request->input('status');
        $rideRequest->user_id = $request->input('user_id');
        $rideRequest->user_comment = $request->input('user_comment');
        // return RideResource::collection($ride);
        if ($ride->save()) {
            $message['data']['RideRequest'] =new RideRequestResource ($ride);
            $message['code'] = 200;
            $message['message'] = "update success";
        }
        $test = 90 ;

        // test
        return $message;
    }

    public function statusDriverTripRequest(Request $request){

        $rideRequest = RideRequestModel::findOrFail($request->input('id'));


        $rideRequest->status = $request->input('status');
        $rideRequest->user_id = $request->input('driver_id');
        $rideRequest->user_comment = $request->input('driver_comment');
        // return RideResource::collection($ride);
        if ($ride->save()) {
            $message['data']['RideRequest'] =new RideRequestResource ($ride);
            $message['code'] = 200;
            $message['message'] = "update success";
        }

        return $message;
    }
}
