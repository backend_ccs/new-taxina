<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Offer;
use App\Bakat;

class copounController extends Controller
{
    public $message = array();

    public function show_promotions(Request $request){

        if(auth()->User()){
             $select = \App\User_offer::select('user_offer.id','offers.title','end_date','value','promo_code','user_offer.state','user_offer.created_at')
                    		    ->join('offers','user_offer.offer_id','=','offers.id')
                    		    ->where('user_offer.user_id' , auth()->User()->id)->get();
    
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "جميع العروض";
                    $msg_error  =  "لا يوجد عروض";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "all the promotions";
                    $msg_error = "there is no promotions"; 
                    $msg_token = "Token is not provided";   
                }
                
        if( count($get_data) >0 ){
            $message['data'] = $get_data;
            $message['error'] = 0;
            $message['message'] = $msg_data;
        }else{
            $message['data'] = $get_data;
            $message['error'] = 1;
            $message['message'] = $msg_error;
        }
                
        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);

    }
    
    
    public function check_promoCode(Request $request){
        
        if(auth()->User()){
            $promocode = $request->input('promo_code');
            
            $updated_at = carbon::now()->toDateTimeString();
            $date = date('Y-m-d',strtotime('+2 hours',strtotime($updated_at)));
       
            $check = \App\Offer::where([['promo_code' , $promocode], ['end_date' ,'>=' , $date]])->value('value');
            
            $get_promoid = \App\Offer::where([['promo_code' , $promocode], ['end_date' ,'>=' , $date]])->value('id');
            
            $check_validation = \App\User_offer::where([['user_id' , auth()->User()->id] , ['offer_id' , $get_promoid] , ['state' , 'open'] ])->value('id');
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "الرمز متوفر ";
                    $msg_error  =  "الرمز غير متوفر";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "Promo code is avaliable";
                    $msg_error =  "Promo code is Not avaliable";
                    $msg_token = "Token is not provided";   
                }
                
            
            if( $check != NULL && $check_validation != NULL){
                $message['data'] = $check;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $check;
                $message['error'] = 1;
                $message['message'] =$msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
    
    
    //*****************************************dashboard offers *********//
    
    
    
    
    public function show_alloffers(Request $request){
	     if(Auth()->User()){
               $offer = Offer::select('id','title','end_date','value','promo_code','created_at')->get();

	     	if(count($offer)>0){
     	 	   $message['data']=$offer;
               $message['error']=0;
               $message['message']='show all offer';
     	 	}else{
               $message['data']=$offer;
               $message['error']=1;
               $message['message']='no offers yet';
     	 	}
     	 }else{
      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
        return response()->json($message);
	}
	
	public function show_offerByid(Request $request){
	    if(Auth()->User()){
	         $id=$request->input('id');

	         $offer=Offer::select('id','title','end_date','value','promo_code')->where('id',$id)->first();

		     	if( $offer !=null){
	               $message['data']=$offer;
	               $message['error']=0;
	               $message['message']='show  offer';
	     	 	}else{
	               $message['data']=NULL;
	               $message['error']=1;
	               $message['message']='no offer yet';
	     	 	}
            }else{
          	    $message['error'] = 2;
                $message['message'] = 'this token is not provided';
            }
        return response()->json($message);
    }
    
    public function delete_offer(Request $request){
       if(Auth()->User()){
            $id=$request->input('id');

            $offer=Offer::where('id',$id)->delete();
  	     	if($offer == true){
       	 	      $message['error']=0;
                  $message['message']='delete offer success';
       	 	}else{
                 $message['error']=1;
                 $message['message']='error in delete offer';
       	 	}
         }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
       return response()->json($message);
	   
	}
	
	public function insert_offer(Request $request){
       if(Auth()->User()){

    	  $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
 
           $insert=new Offer;

	       $insert->title = $request->input('title');
	       $insert->end_date = $request->input('end_date');
           $insert->value = $request->input('value');
           $insert->promo_code = $request->input('promo_code');
	       $insert->created_at = $dateTime;
	       $insert->save();
	         
	     	if($insert == true){
     	       $message['error']=0;
               $message['message']='insert offer success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in insert offer';
     	 	}
          
        }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
	       
	}
	
	public function update_offer(Request $request){
	     if(Auth()->User()){

	          
	          $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          	  $id=$request->input('id');   

	         $update=Offer::where('id',$id)->update([
                                          'title'=>$request->input('title'),
                                          'value'=>$request->input('value'),
                                          'promo_code'=>$request->input('promo_code'),
                                          'end_date'=>$request->input('end_date'),
                                          'updated_at'=>$dateTime
                            
                            	         ]);
                            	         
	     	if($update == true){
     	 	   $message['error']=0;
               $message['message']='update offer success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in updtea offer';
     	 	}
          
        }else{
      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
       }


    //************************bakat ****************/
    
    
    public function show_allbakat(Request $request){
       if(Auth()->User()){

            $baka=Bakat::select('id','name','image','start_price','price','created_at')->get();
    
            if(count($baka)>0){
              
                  $message['data']=$baka;
                  $message['error']=0;
                   $message['message']='show all bakat';
            }else{
                  $message['data']=$baka;
                  $message['error']=1;
                   $message['message']='no bakat yet';
            }
       }else{

            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }

     return response()->json($message);
      
    }
  
    public function show_bakaByid(Request $request){
       if(Auth()->User()){

             $id=$request->input('id');

             $baka=Bakat::select('id','name','image','start_price','price','created_at')->where('id',$id)->first();


            if( $baka !=null){
              
                  $message['data']=$baka;
                  $message['error']=0;
                   $message['message']='show  baka';
            }else{
                  $message['data']=NULL;
                  $message['error']=1;
                   $message['message']='no baka yet';
            }
              }else{

              $message['error'] = 2;
              $message['message'] = 'this token is not provided';
              }
        return response()->json($message);
    }

    public function delete_baka(Request $request){
        if(Auth()->User()){

           $id=$request->input('id');

           $baka=Bakat::where('id',$id)->delete();
         
          if($baka == true){
              $message['error']=0;
              $message['message']='delete baka success';
          }else{
                $message['error']=1;
                $message['message']='error in delete baka';
          }
         }else{

            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
      return response()->json($message);
    }
  
  
  
  
  
  public function insert_baka(Request $request){
       if(Auth()->User()){

            
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
      
           $image=$request->file('image');
           $name =$request->input('name');
           
           if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                $destinationPath_id = 'uploads/bakat';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/bakat/".$savedFileName;
            }else{
                $images = "";
            }     

          $check_name=Bakat::where('name',$name)->first();

          if($check_name !=null){
             $message['error']=4;
             $message['message']='this name exist';
            
          }else{

           $insert=new \App\Bakat;
           $insert->name = $name;
           $insert->image = $images;
           $insert->start_price = $request->input('start_price');
           $insert->price = $request->input('price');
           $insert->created_at = $dateTime;
           $insert->save();
           
            if($insert == true){
                   $message['error']=0;
                   $message['message']='insert baka success';
            }else{
                   $message['error']=1;
                   $message['message']='error in insert baka';
            }
          }
          
        }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
       return response()->json($message);
   }
   
   
    public function update_baka(Request $request){
        if(Auth()->User()){
            
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
   
            $id=$request->input('id');   
            $image=$request->file('image');
            $name = $request->input('name');
            
            $check_image = Bakat::where('id',$id)->value('image');
            
          if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                $destinationPath_id = 'uploads/bakat';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/bakat/".$savedFileName;
            }else{
                if($check_image != NULL){
                    $images = $check_image;
                }else{
                    $images = "";
                }
                
            }     

        $check_name=Bakat::where([['name',$name],['id','!=',$id]])->first();
    
          if($check_name !=null){
             $message['error']=4;
             $message['message']='this name exist';
            }else{
              
               
               $update=Bakat::where('id',$id)->update([
                  'name'=> $name,
                  'image'=>$images,
                  'start_price' => $request->input('start_price'),
                  'price'=>$request->input('price'),
                  'updated_at'=>$dateTime
    
               ]);
               
               
            if($update == true){
                   $message['error']=0;
                   $message['message']='update baka success';
            }else{
                   $message['error']=1;
                   $message['message']='error in updtea baka';
            }
        }
          
        }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
     return response()->json($message);
         
  }

}
