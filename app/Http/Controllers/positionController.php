<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Carbon\Carbon;

use JWTFactory;
use JWTAuth;
use Validator;
use Response;

class positionController extends Controller
{
    public $message = array();
    
    
    public function show_all_city(Request $request){
        if(auth()->User()){
            
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "جميع المدن";
                    $msg_error  = "لايوجد رحلات ";
                }else{
                    $msg_data = "all city data";
                    $msg_error = "No special Trip";
                }
            
            $get_country = \App\User_country::where('user_id' , auth()->User()->id)->value('country_id');
            
            $get_data = \App\City::select('id', 'name','created_at', 'updated_at')->where('country_id' , $get_country)->get();
                
                
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] =$msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    public function show_area_ByID(Request $request){
        if(auth()->User()){
            
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "جميع المناطق";
                    $msg_error  = "لايوجد رحلات ";
                }else{
                    $msg_data = "all area data";
                    $msg_error = "No special Trip";
                }
                
            $city_id = $request->input('city_id');
            
            $get_data = \App\Area::where('city_id' , $city_id)->get();

                
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] =$msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    public function show_area_city(Request $request){
        if(auth()->User()){
             
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "جميع المناطق";
                    $msg_error  = "لايوجد مناطق ";
                }else{
                    $msg_data = "all area data";
                    $msg_error = "No area data";
                }
            
            $country_id = $request->input('country_id');
            
            $get_data  = \App\Area::select('area.id as area_id', 'area.name as area_name', 'city.name as city_name')
                                 ->join('city' , 'area.city_id' ,'='  ,'city.id')
                                 ->where('city.country_id' , $country_id)->get(); 
                                 
                                 
            if( count( $get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data; 
                $message['error'] = 1;
                $message['message']  = $msg_error;
            } 
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
}
?>