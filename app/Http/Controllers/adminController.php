<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

use carbon\carbon;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use App\aboutUs;
use App\Car_type;
use App\Car_category;
use Illuminate\Support\Str;
use Hash;


class adminController extends Controller
{
    public $message=array();

  public function admin_login(Request $request){
   
          
        try{
        
          
             if ( $token = JWTAuth::attempt( ["email" => $request->input('email') , "password" => $request->input('password') , "role" => '1'])) {
                
                
                
                $data = \App\User::where('id',Auth()->User()->id)->first();;

                $data['token'] = $token;

                $message['data'] =$data;
                $message['error'] = 0;
                $message['message'] = 'login success';
                return response()->json( $message,200);

            }else{
                $message['data'] = NULL;
                $message['error'] = 1;
                $message['message'] ='data is wrong';
            }

            
        }catch(JWTException $e){

            return response()->json( ['error'=> 'could not create token'],500);
        }
   
   
           return response()->json( $message);
    } 
    
    
     public function update_admin_profile(Request $request){

        if( Auth()->User()){
          
            
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
            
            $image = $request->file('image');

            $check_image = \App\User::select('first_name','last_name','image')->where('id' , Auth()->User()->id)->first();

            if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/users';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/users/".$savedFileName;
            }else{
                if( $check_image != NULL){
                    $images = $check_image->image;
                }else{
                    $images = NULL;
                }
            }
            
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            
            if( $request->has('first_name') && $first_name != NULL){
                $firstname = $first_name;
            }else{
                $firstname = $check_image->first_name;
            }


            if( $request->has('last_name') && $last_name != NULL){
                $lastname = $last_name;
            }else{
                $lastname = $check_image->last_name;
            }

            $update_data = \App\User::where('id' , Auth()->User()->id)
                                    ->update([
                                        "first_name" => $firstname,
                                        "last_name" => $lastname ,
                                        "email" => $request->input('email'),
                                        "phone" => $request->input('phone'),
                                        "image" => $images,
                                        "updated_at" => $dateTime
                                    ]);
                                    
             $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                

            if( $update_data == true){
                $message['error'] = 0;
                $message['message'] = "profile is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "error, please try again";
            }
        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;


        }
        return response()->json($message);

        
    }
    
    
     public function show_all_users(Request $request){
        try{
            
             if( Auth()->User()){
    
             $data = User::where('users.role',2)->get();
             
              if(count($data)>0 ){
    
                $message['data']=$data;
                $message['error']=0;
                $message['message']='show all user data';
              }else{
    
                $message['data']=$data;
                $message['error']=1;
                $message['message']='no data exist';
    
              }
          }else{
          	    $message['error'] = 2;
                $message['message'] = 'this token is not provided';
          }
    	}catch(Exception $ex){
               $message['error']=3;
               $message['message']='error'.$ex->getMessage();
            }
        return response()->json($message);

    } 
    
    
    public function show_all_drivers(Request $request){
        try{
            
             if( Auth()->User()){
    
                 $select=User::select('users.id', 'first_name', 'last_name', 'email', 'phone','gender' ,'image','rate', 'role','is_online', 'is_driver', 'is_busy', 'wallet','target','users.created_at' ,'country.name as country_name')
                             ->join('user_car' , 'users.id' , '=' , 'user_car.user_id')
                             ->leftjoin('user_country' , 'users.id' ,'=' ,'user_country.user_id')
                             ->leftjoin('country' , 'user_country.country_id' ,'=' ,'country.id')
                             ->where([['user_car.status' , 'accept'], ['users.role',3]])->orderby('id' , 'ASC')->get();
        
        
                  if(count($select)>0 ){
        
                    $message['data']=$select;
                    $message['error']=0;
                    $message['message']='show all drivers data';
                  }else{
        
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data exist';
        
                  }
              }else{
        
              	    $message['error'] = 2;
                    $message['message'] = 'this token is not provided';
              }


    	}catch(Exception $ex){
               $message['error']=3;
               $message['message']='error'.$ex->getMessage();
            }
        return response()->json($message);

    }
    
    
    public function show_drivers_refusedcar(Request $request){
        try{
            
             if( Auth()->User()){
    
                 $select=User::select('users.id', 'first_name', 'last_name', 'email', 'phone','gender' ,'image','rate', 'role','is_online', 'is_driver', 'is_busy', 'wallet','target','users.created_at','country.name as country_name')
                             ->join('user_car' , 'users.id' , '=' , 'user_car.user_id')
                             ->leftjoin('user_country' , 'users.id' ,'=' ,'user_country.user_id')
                             ->leftjoin('country' , 'user_country.country_id' ,'=' ,'country.id')
                             ->where([['user_car.status' , 'refuse'], ['users.role',3]])->orderby('id' , 'ASC')->get();
        
        
                  if(count($select)>0 ){
        
                    $message['data']=$select;
                    $message['error']=0;
                    $message['message']='show all drivers with car refused data';
                  }else{
        
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data exist';
        
                  }
              }else{
        
              	    $message['error'] = 2;
                    $message['message'] = 'this token is not provided';
              }


    	}catch(Exception $ex){
               $message['error']=3;
               $message['message']='error'.$ex->getMessage();
            }
        return response()->json($message);

    }
    
    
      public function show_drivers_needcar(Request $request){
        try{
            
             if( Auth()->User()){
    
                 $select=User::select('users.id', 'first_name', 'last_name', 'email', 'phone','gender' ,'image','rate', 'role','is_online', 'is_driver', 'is_busy', 'wallet','target','users.created_at' ,'country.name as country_name')
                             ->join('user_car' , 'users.id' , '=' , 'user_car.user_id')
                             ->leftjoin('user_country' , 'users.id' ,'=' ,'user_country.user_id')
                             ->leftjoin('country' , 'user_country.country_id' ,'=' ,'country.id')
                             ->where([['user_car.status' , 'need'], ['users.role',3]])->orderby('id' , 'ASC')->get();
        
        
                  if(count($select)>0 ){
        
                    $message['data']=$select;
                    $message['error']=0;
                    $message['message']='show all drivers with car refused data';
                  }else{
        
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data exist';
        
                  }
              }else{
        
              	    $message['error'] = 2;
                    $message['message'] = 'this token is not provided';
              }


    	}catch(Exception $ex){
               $message['error']=3;
               $message['message']='error'.$ex->getMessage();
            }
        return response()->json($message);

    }
    public function show_aboutUS(Request $request){
        try{
            
         if( Auth()->User()){

         $select=aboutUs::select('name','time_price','present','bus_present')->where('id',1)->first();

          if($select !=null ){

            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data success';
          }else{

            $message['data']=NULL;
            $message['error']=1;
            $message['message']='no data';

          }
      }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
      }


    	}catch(Exception $ex){
               $message['error']=3;
               $message['message']='error'.$ex->getMessage();
            }
        return response()->json($message);

    }
    
    public function update_aboutUS(Request $request){

            
         if( Auth()->User()){

          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));



         $update=aboutUs::where('id','1')->update([
                                            'name'=>$request->input('name'),
                                            'time_price' => $request->input('time_price'),
                                            'present'=>$request->input('present'),
                                            'bus_present'=>$request->input('bus_present'),
                                            'updated_at'=>$dateTime

                                       ]);


          if($update ==true ){
            $message['error']=0;
            $message['message']='update data success';
          }else{
            $message['error']=1;
            $message['message']='error in update data';

          }
      }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
      }


        return response()->json($message);

    }
    
    
    // trip s*****************************************************************************************************
    
    
    
    
    public function show_needTrip(Request $request){
        if( Auth()->User()){

         $select= \App\Trip::select('trip.id', 'user_car.car_name', 'client.first_name as user_firstname', 'client.last_name as user_lastname', 'driver.first_name as driver_firstname','driver.last_name as driver_lastname', 'trip.start_lat', 'trip.start_long', 'trip.start_location',
                                'trip.end_lat', 'trip.end_long', 'trip.end_location', 'trip.distance', 'trip.promo_code', 'trip.price', 'trip.note', 'trip.rate', 'trip.status', 'trip.pick_time', 'trip.payment_method', 'trip.type', 'trip.date', 'trip.time', 'trip.created_at', 'trip.updated_at')
                      ->join('users as client','trip.user_id','=','client.id')
                      ->join('users as driver','trip.driver_id','=','driver.id')
                      ->leftjoin('user_car','trip.userCar_id','=','user_car.id')
                      ->where('trip.status','need')->get();

          if(count($select)>0 ){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='all need trip';
          }else{
            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data exist';
          }
      }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
      }
        return response()->json($message);
    }
    
    
    

    public function show_endTrip(Request $request){
        if( Auth()->User()){
             $select= \App\Trip::select('trip.id', 'user_car.car_name', 'client.first_name as user_firstname', 'client.last_name as user_lastname', 'driver.first_name as driver_firstname','driver.last_name as driver_lastname', 'trip.start_lat', 'trip.start_long', 'trip.start_location',
                                'trip.end_lat', 'trip.end_long', 'trip.end_location', 'trip.distance', 'trip.promo_code', 'trip.price', 'trip.note', 'trip.rate', 'trip.status', 'trip.pick_time', 'trip.payment_method', 'trip.type', 'trip.date', 'trip.time', 'trip.created_at', 'trip.updated_at')
                      ->join('users as client','trip.user_id','=','client.id')
                      ->join('users as driver','trip.driver_id','=','driver.id')
                      ->leftjoin('user_car','trip.userCar_id','=','user_car.id')
                      ->where('trip.status','end')->get();;


          if(count($select)>0 ){

            $message['data']=$select;
            $message['error']=0;
            $message['message']='all end trip';
          }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data exist';

          }
      }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
      }
       
        return response()->json($message);

    }
    
    
    
    
     public function show_acceptTrip(Request $request){
        if( Auth()->User()){
             $select= \App\Trip::select('trip.id', 'user_car.car_name', 'client.first_name as user_firstname', 'client.last_name as user_lastname', 'driver.first_name as driver_firstname','driver.last_name as driver_lastname', 'trip.start_lat', 'trip.start_long', 'trip.start_location',
                                'trip.end_lat', 'trip.end_long', 'trip.end_location', 'trip.distance', 'trip.promo_code', 'trip.price', 'trip.note', 'trip.rate', 'trip.status', 'trip.pick_time', 'trip.payment_method', 'trip.type', 'trip.date', 'trip.time', 'trip.created_at', 'trip.updated_at')
                      ->join('users as client','trip.user_id','=','client.id')
                      ->join('users as driver','trip.driver_id','=','driver.id')
                      ->leftjoin('user_car','trip.userCar_id','=','user_car.id')
                      ->where('trip.status','accept')->get();;


          if(count($select)>0 ){

            $message['data']=$select;
            $message['error']=0;
            $message['message']='all accepted trip';
          }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data exist';

          }
      }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
      }
        return response()->json($message);

    }
    
    
    public function counts(){
        if(auth()->User()){
            
            $all_users = \App\User::where('role' , 2)->count();
            $all_drivers = \App\User::where('role' , 3)->count();
            $trip = \App\Trip::count();
            $special = \App\Special_trip::count();
            $promotion = \App\Offer::count();
            
            
            $message['user_num'] = $all_users;
            $message['driver_num'] = $all_drivers;
            $message['trip_num'] = $trip;
            $message['special_num'] = $special; 
            $message['copoun_num'] = $promotion;
            

        }else{
            $message['error'] = 2;
            $message['message'] = 'this token is not provided';
      }
        return response()->json($message);
        
    }
    
}
?>