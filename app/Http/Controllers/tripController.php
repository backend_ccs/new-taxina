<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Carbon\Carbon;

class tripController extends Controller
{
    public $message = array();

    public function User_complete_trip(Request $request){
        if(auth()->User()){
             $msg_data ="";
            $msg_error ="";
            $msg_token = "";
            $specialData = array();
        
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =   "الرحلات السابقة";
                    $msg_error  =  "لا توجد رحلات";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "All completed trip";
                    $msg_error ="No completed trip";
                    $msg_token = "Token is not provided";   
                }

            $get_data = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.driver_id' ,'='  ,'users.id')
                                ->where([['status' , 'end'], ['user_id' , auth()->user()->id] ])->get();

            

            $data = \App\Car_seat::select('specialTrip_id')->join('special_trip' , 'car_seats.specialTrip_id' ,'=' ,'special_trip.id')
                                ->where([['car_seats.user_id' , auth()->User()->id] , ['special_trip.state' , 'end']])->distinct()->get();
             
            foreach($data  as $special){
                
                array_push($specialData ,  \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price' , 'car_seats.money','user_car.car_name','user_car.plate', 'user_car.color' , 'user_car.type_id')
                                         ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                         ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                         ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                         ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                         ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                         ->join('car_seats' , 'special_trip.id' , '=', 'car_seats.specialTrip_id')
                                        ->where([['special_trip.state' , 'end'], ['special_trip.id' , $special->specialTrip_id]])->first());
                                      
               
            } 
                                     
                                     
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['special_trip'] = $specialData;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;    
                $message['special_trip'] = $specialData;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
    
     public function User_upcoming_trip(Request $request){
        if(auth()->User()){

             $msg_data ="";
            $msg_error ="";
            $msg_token ="";
            $specialData = array();
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =   "الرحلات القادمة";
                    $msg_error  =  "لا توجد رحلات";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "All Upcoming trip";
                    $msg_error ="No Upcoming trip";
                    $msg_token = "Token is not provided";   
                }
            $get_data = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.user_id' ,'='  ,'users.id')
                                ->where([['status' , 'accept'], ['user_id' , auth()->user()->id]])->get();
                                
            
            $data = \App\Car_seat::select('specialTrip_id')->join('special_trip' , 'car_seats.specialTrip_id' ,'=' ,'special_trip.id')
                                ->where([['car_seats.user_id' , auth()->User()->id] , ['special_trip.state' , 'wait']])->distinct()->get();
             
            foreach($data  as $special){
                
                array_push($specialData ,  \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price' , 'car_seats.money','user_car.car_name','user_car.plate', 'user_car.color' , 'user_car.type_id')
                                         ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                         ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                         ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                         ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                         ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                         ->join('car_seats' , 'special_trip.id' , '=', 'car_seats.specialTrip_id')
                                        ->where([['special_trip.state' , 'wait'], ['special_trip.id' , $special->specialTrip_id]])->first());
                                      
               
            } 

            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['special_trip'] = $specialData;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data; 
                $message['special_trip'] = $specialData;
                $message['error'] = 1;
                $message['message_en'] =$msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;
        }
        return response()->json($message);
    }
    
     public function User_canceled_trip(Request $request){
        if(auth()->User()){
             $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =   "الرحلات السابقة";
                    $msg_error  =  "لا توجد رحلات";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "All completed trip";
                    $msg_error ="No completed trip";
                    $msg_token = "Token is not provided";   
                }

            $get_data = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.user_id' ,'='  ,'users.id')
                                ->where([['status' , 'cancel'], ['user_id' , auth()->user()->id] ])->get();

            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
   
   
    public function User_state_trip(Request $request){
        if(auth()->User()){
             $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =   "الرحلات السابقة";
                    $msg_error  =  "لا توجد رحلات";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "All completed trip";
                    $msg_error ="No completed trip";
                    $msg_token = "Token is not provided";   
                }

            $completed = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.user_id' ,'='  ,'users.id')
                                ->where([['status' , 'end'], ['user_id' , auth()->user()->id] ])->get();
                                
                                
            $accept = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.user_id' ,'='  ,'users.id')
                                ->where([['status' , 'accept'], ['user_id' , auth()->user()->id]])->get();
                                
                                
            $cancel = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.user_id' ,'='  ,'users.id')
                                ->where([['status' , 'cancel'], ['user_id' , auth()->user()->id] ])->get();

            if( count($completed)>0 && count($accept)>0  && count($cancel)>0){
                $message['completed_trip'] = $completed;
                $message['accept_trip'] = $accept;
                $message['cancel_trip'] = $cancel;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['completed_trip'] = $completed;
                $message['accept_trip'] = $accept;
                $message['cancel_trip'] = $cancel;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
   
   
   
   
     public function Driver_requested_trip(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";
            $msg_error2 ="";
            $data = array();
            
            $longitude = $request->input('longitude');
            $latitude = $request->input('latitude');
            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =   "الرحلات القادمة";
                    $msg_error  =  "لا توجد رحلات";
                    $msg_error2 ="انت معك رحله الان";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "All requested trip";
                    $msg_error ="No requested trip";
                    $msg_error2 ="you are busy now";
                    $msg_token = "Token is not provided";   
                }

            $check = \App\User::where([['id' , auth()->User()->id] ,['is_busy' , '0'] ])->first();
            
            if( $check != NULL){
                
                
                $get_trips = \App\Driver_trip::select('trip_id')->where('driver_id' , auth()->user()->id)->orderBy('created_at' , 'DESC')->get();
                
                
                foreach($get_trips as $trip){
                      $x =  \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','users.phone','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'duration_time', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                        ->join('users' , 'trip.user_id' ,'=', 'users.id')
                                        ->where([['status' , 'need'], ['trip.id' , $trip->trip_id], ['type' , '0'] ])->first();
                                        
                    
                    if( $x != NULL){
                        array_push($data , $x);
                    }
                }
                
                $update_location = \App\User::where('id' , auth()->User()->id)->update(['Latitude' => $latitude , 'Longitude' => $longitude ]);

                // $get_trip = DB::select("SELECT id , ( 6371 * acos( cos( radians($latitude) ) * cos( radians( `start_lat` ) ) * cos( radians( `start_long` ) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians( `start_lat` ) ) ) ) AS distance FROM `trip` where  status = 'need'  ORDER BY distance ASC");

                // foreach($get_trip as $trip_id){
                //     $get_data = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                //                                     'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                //                         ->join('users' , 'trip.user_id' ,'=', 'users.id')
                //                         ->where([['status' , 'need'], ['driver_id' , auth()->user()->id], ['type' , '0'],['trip.id' , $trip_id->id] ])->first();
                                        
                //     if($get_data != NULL){
                //         array_push($data ,$get_data );
                //     }

                // }

                if( count($data)>0 ){
                    $message['data'] = $data;
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['data'] = $data;
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
                }else{
                $message['data'] = [];
                $message['error'] = 1;
                $message['message'] = $msg_error2;
                }


                }else{
                $message['error'] = 2;
                $message['message'] = $msg_token;

                }
        return response()->json($message);
    }
    
    
    public function accept_trip( Request $request){
        if( auth()->user()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_error2="";
            $msg_token ="";

            $created_at = carbon::now()->toDateTimeString();
             $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

          
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "تم قبول الرحلة بنجاح";
                    $msg_error  =   "يوجد خطأ حاول مره اخرى";
                    $msg_error2 = "انت معك رحله بالفعل";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "trip is accepted successfuly";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 = "you already have a trip";
                    $msg_token = "Token is not provided";   
                }
                
            $trip_id = $request->input('trip_id');
            $pick_time = $request->input('pick_time');
            
            $get_user = \App\Trip::where('id' , $trip_id)->value('user_id');
            
            $check = \App\User::where([['id'  , auth()->User()->id] , ['is_busy' , '1']])->first();
            
            if( $check != NULL){
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }else{
                     
             
                $updateUser = \App\User::where([['id' , auth()->User()->id] ])->update(['is_busy' => '1'] );
                
                $update_tripRequest = \App\Driver_trip::where([['trip_id' , $trip_id], ['driver_id' , auth()->User()->id]])->update(['state' => 'accept']);
                              
                $update_trip_Request = \App\Driver_trip::where([['trip_id' , $trip_id], ['driver_id' , '!=' , auth()->User()->id]])->update(['state' => 'end']);

                $updatess = \App\Trip::where([['id' , $trip_id], ['status' , 'need']] )->update(['status' =>'accept' , 'driver_id' => Auth()->User()->id , 'pick_time' => $pick_time]);

                
                
                $get_userData = \App\User::where('id' ,$get_user )->first();
                
                define( 'API_ACCESS_KEY12', 'AAAAx0JkJkg:APA91bF4mFym1nM37kLnaTqgDnUVBqbhp41Xx5GH978IoJUT9MSLh03T6DxfB98R2V5SXgeiEQ0LkB0ZGZ7cOv6m8JSaxWIu-BJOYceF6r1PFxG022AfwFLP-l_T86l4aUjyqONFZ0tt');
    
    
                        #prep the bundle
                             
                        	$msg = array(
                            		'body' 	=> "تم قبول الرحلة بنجاح" ,
                            		'title'	=>  "Taxina Trip",
                                    'click_action' => "1"
                                      );
                            	$fields = array
                            			(
                            				'to'		=> $get_userData->firebase_token,
                                            'data' => $mg = array(
                                                         'redirect_id'  => "$trip_id",
                                                         'type' => "1",
                                                         'status' => "1",
                                                    ),
                            				'notification'	=> $msg
                            			);
                        	                 
    
                        	$headers = array
                        			(
                        				'Authorization: key=' . API_ACCESS_KEY12,
                        				'Content-Type: application/json'
                        			);
                        #Send Reponse To FireBase Server	
                        		$ch = curl_init();
                        		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        		curl_setopt( $ch,CURLOPT_POST, true );
                        		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        		$result = curl_exec($ch );
                        		//echo $result;
                        		curl_close( $ch );
                         
                         $add_notify = new \App\Notification;
                            	
                    	$add_notify->body = "تم قبول الرحلة بنجاح" ;
                    	$add_notify->title =  "Taxina Trip";
                    	$add_notify->click_action = 1;
                    	$add_notify->redirect_id = $trip_id;
                    	$add_notify->type = 1;
                    	$add_notify->status = 1;
                    	$add_notify->user_id = $get_user;
                    	$add_notify->created_at = $dateTime;
                    	$add_notify->save();
                         


                if( $updatess == true  ){
                    $message['data'] = $get_userData;
                    $message['error'] = 0 ;
                    $message['message'] = $msg_data;
                }else{
                    $message['data'] = $get_userData;
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }
        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
    
    
    
    
    public function show_driver_completedtrip(Request $request){
        if(auth()->user()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "الرحلات المنتهية ";
                    $msg_error  = "لايوجد رحلات ";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "all ended trips";
                    $msg_error = "No ended Trip";
                    $msg_token = "Token is not provided";   
                }
            $get_data = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.user_id' ,'='  ,'users.id')
                                ->where([['status' , 'end'], ['driver_id' , auth()->user()->id] ])->get();
            
            
            $total_earned = DB::select("SELECT count(id) as count ,sum(price) as earned FROM `trip` WHERE MONTH(CURRENT_DATE()) and driver_id = ?",[auth()->user()->id]);

            if( count($get_data)>0){
                $message['data'] = $get_data;
                $message['trip_count'] = $total_earned[0]->count;
                $message['earned'] = $total_earned[0]->earned;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
        }else{
            $message['error'] = 2;
            $message['message'] = $msg_token;

        }
        return response()->json($message);
    }
    
    
    
    public function  show_urTrip_nw(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "الرحلات المنتهية ";
                    $msg_error  = "لايوجد رحلات ";
                }else{
                    $msg_data =  "all ended trips";
                    $msg_error = "No ended Trip";
                }
                
            $get_trip = \App\Trip::where([['driver_id' , auth()->User()->id] , ['status' , 'accept']])->value('id');
            
            $message['trip_id'] = $get_trip;
                
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    public function show_trip_ByID(Request $request){
        if( auth()->User()){
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "بيانات الرحلة";
                    $msg_error  = "لايوجد رحلات ";
                }else{
                    $msg_data =  "Trip data";
                    $msg_error = "No Trip data";
                }
            
            $trip_id = $request->input('trip_id');
            $type = $request->input('type');
            
            if( $type == 1){
            
                $get_state = \App\Trip::where('id' , $trip_id )->value('status');
                
                if( $get_state == 'need' ){
                    $state = 0;
                }elseif( $get_state == 'accept'){
                    $state = 1;
                }elseif( $get_state == 'end'){
                    $state = 2;
                }elseif( $get_state == 'cancel'){
                    $state = 3;
                }
                $get_data = \App\Trip::select('trip.id', 'trip.driver_id', 'users.first_name', 'users.last_name', 'users.phone', 'users.image', 'user_car.car_name','user_car.plate', 'user_car.color','trip.start_lat', 'trip.start_long', 'trip.start_location', 'trip.end_lat', 'trip.end_long', 
                                                'trip.end_location', 'trip.distance', 'trip.promo_code', 'trip.price', 'trip.note', 'trip.rate', 'trip.pick_time', 'trip.code', 'trip.type', 'trip.date', 'trip.time', 'trip.created_at')
                                    ->leftjoin('users' , 'trip.driver_id' ,'=' ,'users.id')
                                    ->leftjoin('user_car' ,'trip.driver_id' ,'=' ,'user_car.user_id')
                                    ->where('trip.id' , $trip_id)->first();
                       
                $get_data['state'] =  $state;            
            }else{
                
                
                $get_state = \App\Special_trip::where('id' , $trip_id)->value('state');
                
                if( $get_state == 'wait'){
                    $state = 0;
                }elseif( $get_state == 'start'){
                    $state = 1;
                }elseif( $get_state == 'end'){
                    $state = 2;
                }
             
                 $get_data = \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price'
                                                       ,'special_trip.code' ,'user_car.car_name','user_car.plate', 'users.id as driver_id','user_car.color','users.first_name', 'users.last_name', 'users.phone', 'users.image', 'users.rate')
                                             ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                             ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                             ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                             ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                             ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')     
                                             ->leftjoin('users' , 'special_trip.user_id' ,'=' ,'users.id')
                                             ->where('special_trip.id' , $trip_id)->first();
                 
                 $get_data['state'] =    $state;                         
                                         
            }    
            if( $get_data != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data; 
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }                    
                                
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    
     public function show_tripDetails_ById(Request $request){
        if( auth()->User()){
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "بيانات الرحلة";
                    $msg_error  = "لايوجد رحلات ";
                }else{
                    $msg_data =  "Trip data";
                    $msg_error = "No Trip data";
                }
            
            $trip_id = $request->input('trip_id');
            
            $get_data = \App\Trip::select('trip.id', 'trip.user_id', 'users.first_name', 'users.last_name', 'users.phone', 'users.image', 'trip.start_lat', 'trip.start_long', 'trip.start_location', 'trip.end_lat', 'trip.end_long', 
                                            'trip.end_location', 'trip.distance', 'trip.promo_code', 'trip.price', 'trip.note', 'trip.rate', 'trip.pick_time', 'trip.code', 'trip.type', 'trip.date', 'trip.time', 'trip.created_at')
                                ->leftjoin('users' , 'trip.user_id' ,'=' ,'users.id')
                                ->where('trip.id' , $trip_id)->first();
                            
                
            if( $get_data != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data; 
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }                    
                                
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    
    
    public function show_trip_cost(Request $request){
        if(auth()->User()->id){
            
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم تعديل التكلفه بنجاح";
                    $msg_error  = " بوجد خطأ خاول مره أخرى";
                }else{
                    $msg_data =  "trip price is updated";
                    $msg_error = "error, please try again";
                }
                
            $time = $request->input('time');
            $distance = $request->input('distance');
            $trip_id = $request->input('trip_id');
            
            $TimePrice = \App\aboutUs::where('id' , '1')->value('time_price');
            
            $totalTime = $time * $TimePrice;
            
            $distancePrice = \App\aboutUs::where('id' , '1')->value('present');

            $totalDistance = $distance * $distancePrice ;
            
            
            $total_trip = $totalTime + $totalDistance;
            
            $update = \App\Trip::where('id', $trip_id)->update(['price' => $total_trip]);
        
            $get_present = \App\aboutUs::where('id' , '1')->value('present');
            
            $driver_gain =  ( $total_trip * $get_present ) / 100;
            
            if($update == true){
                $message['your_gain'] = $driver_gain;
                $message['total_price'] = $total_trip;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['your_gain'] = $driver_gain;
                $message['total_price'] = $total_trip;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
             
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    
    
    public function User_waiting_trip(Request $request){
        $msg_token ="";
        if(auth()->User()){

            $msg_data ="";
            $msg_error ="";
            $msg_token ="";
            $specialData = array();
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =   "الرحلات القادمة";
                    $msg_error  =  "لا توجد رحلات";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "All Upcoming trip";
                    $msg_error ="No Upcoming trip";
                    $msg_token = "Token is not provided";   
                }
            $get_data = \App\Trip::select('trip.id', 'userCar_id','user_id','users.first_name' , 'users.last_name', 'users.image','driver_id','start_lat','start_long','start_location','end_lat',
                                                    'end_long','end_location','distance','promo_code','price','note', 'trip.rate', 'trip.status', 'payment_method','type','date','time','trip.created_at')
                                ->join('users' , 'trip.user_id' ,'='  ,'users.id')
                                ->orderBy('trip.id', 'DESC')
                                ->where([['status' , 'need'], ['user_id' , auth()->user()->id]])->get();
                                
                                
            
            $data = \App\Car_seat::select('specialTrip_id')->join('special_trip' , 'car_seats.specialTrip_id' ,'=' ,'special_trip.id')
                                ->where([['car_seats.user_id' , auth()->User()->id] , ['special_trip.state' , 'wait']])->distinct()->get();
             
            foreach($data  as $special){
                
                array_push($specialData ,  \App\Special_trip::select('special_trip.id','special_trip.from_areaId','fromCity.name as fromcity_name', 'fromArea.name as from_area', 'special_trip.to_areaId', 'toCity.name as tocity_name','toArea.name as to_area','seat_count','date','from_time','to_time','price' , 'car_seats.money','user_car.car_name','user_car.plate', 'user_car.color' , 'user_car.type_id')
                                         ->join('user_car' ,'special_trip.user_id' ,'=' ,'user_car.user_id')
                                         ->join('area  as fromArea' , 'special_trip.from_areaId' ,'=' ,'fromArea.id')
                                         ->join('city as fromCity' , 'fromArea.city_id' , '=', 'fromCity.id')
                                         ->join('area  as toArea' , 'special_trip.to_areaId' ,'=' ,'toArea.id')
                                         ->join('city as toCity' , 'toArea.city_id' , '=', 'toCity.id')
                                         ->join('car_seats' , 'special_trip.id' , '=', 'car_seats.specialTrip_id')
                                        ->orderBy('special_trip.id', 'DESC')
                                        ->where([['special_trip.state' , 'wait'], ['special_trip.id' , $special->specialTrip_id]])->first());
                                      
               
            } 

            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['special_trip'] = $specialData;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data; 
                $message['special_trip'] = $specialData;
                $message['error'] = 1;
                $message['message_en'] =$msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token error ";
        }
        return response()->json($message);
    }
    
    
    public function show_UpComingdriver_Trip(Request $request){

        if(Auth()->User()){

            
            $get_data = \App\Trip::where([['status' , 'accept'], ['driver_id' , auth()->user()->id], ['type' , '0'] ])->get();
            
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "رحلاتك المخصوصة ";
                    $msg_error  = "لايوجد رحلات ";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data =  "all special trips";
                    $msg_error = "No special Trip";
                    $msg_token = "Token is not provided";   
                }
            
            
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
}
