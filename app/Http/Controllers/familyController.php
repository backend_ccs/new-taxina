<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Carbon\Carbon;

class familyController extends Controller
{
    public $message = array();


    public function search_phone(Request $request){
        if(auth()->User()){
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "بيانات المستخدم";
                $msg_error = "لا يوجد مستخدم ";
            }else{
                $msg_data = "user data";
                $msg_error = "No user data";
            }
        
            $phone = $request->input('phone');
            
            $get_user = \App\User::select('id', 'first_name', 'last_name')->where('phone' , $phone)->first();
            
            if($get_user != NULL){
                $message['data'] = $get_user;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['data'] = $get_user;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
        }
        return response()->json($message);
    }
    
    
    public function send_familyRequest(Request $request){
        if(auth()->User()){
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
            
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
    
            if($check_setting == 'ar'){
                $msg_data = "تم ارسال طلب تتبع  ";
                $msg_error = "يوجد خطأ حاول مره أخرى";
            }else{
                $msg_data = "Tracking request is send successfully";
                $msg_error = "error, please try again";
            }
        
            $user_id = $request->input('user_id');
            
            $add_request = new \App\FamilyMember;
            
            $add_request->sender_id = auth()->User()->id;
            $add_request->reciever_id = $user_id;
            $add_request->status = "wait";
            $add_request->created_at = $dateTime;
            $add_request->save();    
            
            
            if($add_request == true){
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
        }
        return response()->json($message);
    }
            
    
    public function show_familyMembers_accept(Request $request){
        if(auth()->User()){
            
            $all = array();
            
            $members = \App\FamilyMember::select('family_member.id','users.id as user_id', 'users.first_name', 'users.last_name', 'users.email','users.phone','users.image' )
                                        ->join('users' , 'family_member.reciever_id', '=', 'users.id')
                                        ->where([['family_member.sender_id' , auth()->User()->id] , ['family_member.status' , 'accept'] ])->distinct()->get();
            
            foreach($members as $data){

                $trip = \App\Trip::where([['user_id' ,$data->user_id]] )->first();

                if($trip->status == "accept"){
                    $state = "yes";
                }else{
                    $state= "no";
                }
                array_push($all , (object)array(
                    "id" => $data->id,
                    "user_id" => $data->user_id,
                    "first_name" => $data->first_name,
                    "last_name" => $data->last_name,
                    "email" => $data->email,
                    "phone" => $data->phone,
                    "image" => $data->image,
                    "state" => $state
                    ));
            }                            
                                        
            if(count($members) >0){
                $message['data'] = $all;
                $message['error'] = 0;
                $message['message'] = "all family members"; 
            }else{
                $message['data'] = $all;
                $message['error'] = 1;
                $message['message'] = "you don't have members";
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
        }
        return response()->json($message);
    }    
    
    
    
    public function show_familyMembers_wait(Request $request){
        if(auth()->User()){
            
            $all = array();
            
            $members = \App\FamilyMember::select('family_member.id','users.id as user_id', 'users.first_name', 'users.last_name', 'users.email','users.phone','users.image' )
                                        ->join('users' , 'family_member.reciever_id', '=', 'users.id')
                                        ->where([['family_member.sender_id' , auth()->User()->id] , ['family_member.status' , 'wait']])->distinct()->get();
            
            foreach($members as $data){
                
                $trip = \App\Trip::where([['user_id' ,$data->user_id]] )->first();
                
                if($trip->status == "accept"){
                    $state = "yes";
                }else{
                    $state= "no";
                }
                array_push($all , (object)array(
                    "id" => $data->id,
                    "user_id" => $data->user_id,
                    "first_name" => $data->first_name,
                    "last_name" => $data->last_name,
                    "email" => $data->email,
                    "phone" => $data->phone,
                    "image" => $data->image,
                    "state" => $state
                    ));
            }                            
                                        
            if(count($members) >0){
                $message['data'] = $all;
                $message['error'] = 0;
                $message['message'] = "all family members"; 
            }else{
                $message['data'] = $all;
                $message['error'] = 1;
                $message['message'] = "you don't have members";
            }
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
        }
        return response()->json($message);
    }  
    
    public function accept_request(Request $request){
        if(auth()->User()){
            
            $request_id = $request->input('request_id');
            
            $updateRequest = \App\FamilyMember::where('id' , $request_id)->update(['status' => 'accept']);
            
            if($updateRequest ==  true){
                $message['error'] = 0;
                $message['message'] = "request is accepted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "error, please try again";
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";
        }
        return response()->json($message);
    }
    
}
?>