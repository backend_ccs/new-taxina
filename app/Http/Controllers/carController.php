<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Crypt;
use \App\Car_category;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
            
define( 'API_ACCESS_KEY', 'AAAAx0JkJkg:APA91bF4mFym1nM37kLnaTqgDnUVBqbhp41Xx5GH978IoJUT9MSLh03T6DxfB98R2V5SXgeiEQ0LkB0ZGZ7cOv6m8JSaxWIu-BJOYceF6r1PFxG022AfwFLP-l_T86l4aUjyqONFZ0tt');

class carController extends Controller
{
    public $message = array();


    public function get_suggested_car(Request $request){
    
        if( auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";
            $gender = $request->input('gender');
            $start_long = $request->input('start_long');
            $start_lat = $request->input('start_lat');
            $end_long = $request->input('end_long');
            $end_lat = $request->input('end_lat');
            
            if($gender == "0"){
                $gend = "male";
            }else{
                $gend = "female";
            }
            
             $theta = $start_long - $end_long;
              $dist = sin(deg2rad($start_lat)) * sin(deg2rad($end_lat)) +  cos(deg2rad($start_lat)) * cos(deg2rad($end_lat)) * cos(deg2rad($theta));
              $dist = acos($dist);
              $dist = rad2deg($dist);
              $miles = $dist * 60 * 1.1515;
              //$unit = strtoupper($unit);
            
              $string_dist_others = $miles * 1.609344;
              $float = number_format($string_dist_others,3);
            //   $message['distant miles'] = $float;  // this is kilo meter
            //   $get_data['distance'] = $float;
            
            
            $get_data= array();
            
            $get_drivers = DB::select("SELECT id , ( 6371 * acos( cos( radians($start_lat) ) * cos( radians( `Latitude` ) ) * cos( radians( `Longitude` ) - radians('$start_long') ) + sin( radians('$start_lat') ) * sin( radians( `Latitude` ) ) ) ) AS distance FROM `users` where role = '3' HAVING distance <= '5' ORDER BY distance ASC");

            foreach( $get_drivers as $driver){
                $data = \App\User_car::select('user_car.id', 'car_name','plate','color','brand','sear_num', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.phone', 'users.gender', 'users.image', 'users.rate' , 'user_car.baka_id' ,'bakat.price as baka_price')
                                        ->join('users' , 'user_car.user_id' ,'=' ,'users.id') 
                                        ->leftjoin('bakat' , 'user_car.baka_id', '=', 'bakat.id')
                                        ->where([['users.is_busy' , '0'] , ['users.is_driver' , '1'] , 
                                                 ['users.is_online' , '1'], ['users.gender' , $gend],
                                                 ['users.id' , $driver->id], ['user_car.type_id' , '1']])->first();
                                                 
                                                 
                $total_price = $float * $data->baka_price;

                
                array_push($get_data ,(object)array(
                    
                        "id" => $data->id,
                        "car_name" => $data->car_name,
                        "plate" =>   $data->plate,
                        "color" => $data->color,
                        "brand" =>   $data->brand,
                        "sear_num" => $data->sear_num,
                        "user_id" =>   $data->user_id,
                        "first_name" => $data->first_name,
                        "last_name" =>   $data->last_name,
                        "phone" => $data->phone,
                        "gender" =>   $data->gender,
                        "image"  => $data->image,
                        "rate"  => $data->rate,
                        "price" => round($total_price),
                        "distance" => $float,
                    ));
            }

             

            
            //  $get_data['price'] = round($total_price);
             
             $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = "جميع السائقين المتاحين";
                $msg_token = " من فضلك سجل الدخول";
            }else{
                $msg_data = "this is all drivers";
                $msg_token = "Token is not provided";   
            }
            
            
             $message['data'] = $get_data;
             $message['error'] = 0;
             $message['message'] = $msg_data;

            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);    
    }
    
    
    public function show_suggested_car(Request $request){
        if(auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";

            $type_id = $request->input('type_id');

            $get_data = \App\Car_category::select('id' , 'name')->where('type_id' , $type_id)->get();


             $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = " بيانات انواع العربيات";
                $msg_error  = "لا يوجد بيانات";
                $msg_token = " من فضلك سجل الدخول";
            }else{
                $msg_data = "Car category";
                $msg_error = "No car category";
                $msg_token = "Token is not provided";   
            }
            
            if(is_null($get_data)){
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
    }
    
    
    public function show_carType(Request $request){
        if(auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_token ="";

            $type_id = $request->input('type_id');

            $get_data = \App\Car_type::all();


             $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
            if($check_setting == 'ar'){
                $msg_data = " بيانات انواع العربيات";
                $msg_error  = "لا يوجد بيانات";
                $msg_token = " من فضلك سجل الدخول";
            }else{
                $msg_data = "Car category";
                $msg_error = "No car category";
                $msg_token = "Token is not provided";   
            }
            
            if(is_null($get_data)){
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }else{
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
    }


    public function show_userCar(Request $request){
        if(auth()->User()){
             $msg_data ="";
             $msg_error ="";
             $msg_error2 ="";
             $msg_token ="";
             
            $carCat_id = $request->input('carCat_id');
            $longitude = $request->input('longitude');
            $latitude  = $request->input('latitude');
           // 
            $get_data = array();

            if( $request->has('carCat_id') && $carCat_id != NULL){

                $get_drivers = DB::select("SELECT id , ( 6371 * acos( cos( radians($latitude) ) * cos( radians( `Latitude` ) ) * cos( radians( `Longitude` ) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians( `Latitude` ) ) ) ) AS distance FROM `users` where role = '3' HAVING distance <= '5' ORDER BY distance ASC");

                foreach( $get_drivers as $driver){
                    array_push($get_data , \App\User_car::select('user_car.id', 'car_name','plate','color','brand','sear_num', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.phone', 'users.gender', 'users.image', 'users.rate')
                                            ->join('users' , 'user_car.user_id' ,'=' ,'users.id') 
                                            ->where([['users.is_busy' , '0'] , ['users.is_driver' , '1'] , 
                                                     ['users.is_online' , '1'], ['carCat_id' , $carCat_id],
                                                     ['users.id' , $driver->id]])->first());

                }


                $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = " بيانات انواع العربيات";
                    $msg_error  = "لا يوجد بيانات";
                    $msg_error2 = "من فضلك ادخل نوع العربيه";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "users car";
                    $msg_error = "No car category";
                    $msg_error2 = "please enter car category ";
                    $msg_token = "Token is not provided";   
                }
                
                if( count($get_data) !=0){
                    $message['data'] = $get_data;
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['data'] = $get_data;
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }else{
                $message['data'] = NULL;
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
    }


    public function make_trip(Request $request){

        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";
            $msg_error2 ="";
            $gend ="";
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "نقوم بالبحث عن سواق لرحلتك";
                    $msg_error  = "لا يوجد سائقين بالقرب منك";
                    $msg_error2 = "من فضلك ادخل جميع لبيانات";
                    $msg_error3 = "لا يوجد سواقين بالقرب منك";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "we are processng your Booking...";
                    $msg_error = "No drivers near you";
                    $msg_error2 = "please fill all the fields";
                    $msg_error3 = "No Drivers around U";
                    $msg_token = "Token is not provided";   
                }

        $validation = Validator::make($request->all(), [
            
            "start_lat" => 'required',
            "start_long" => 'required',
            "start_location" => 'required',
            "end_lat" => 'required',
            "end_long" => 'required',
            "end_location" => 'required',
            "distance" => 'required',
        ]);
        
        if( $validation -> fails()){
            $message['error'] = 1;
            $message['message'] = $msg_error2;

            return response()->json($message);
        }
        
        
        $start_lat = $request->input('start_lat');
        $start_long = $request->input('start_long');
        $gender = $request->input('gender');
        $baka_id = $request->input('baka_id');
        
        if( $gender == 0){
            $gend = 'male';
        }else{
            $gend = 'female';
        }
        
        
        $driver = array();
        $send = "";
           
        $duration_time =$request->input('duration_time');
        
        if( $request->has('duration_time') && $duration_time != NULL){
            $duration = $duration_time;
        }else{
            $duration = 0;
        }
            
        $updated_at = carbon::now()->toDateTimeString();
        $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
        
        $add = new \App\Trip;

        $add->user_id = auth()->user()->id;
        $add->start_lat = $start_lat;
        $add->start_long = $start_long;
        $add->start_location = $request->input('start_location');
        $add->end_lat = $request->input('end_lat');
        $add->end_long = $request->input('end_long');
        $add->end_location = $request->input('end_location');
        $add->distance = $request->input('distance');
        $add->promo_code = $request->input('promo_code');
        $add->price = $request->input('price');
        $add->note = $request->input('note');
        $add->duration_time = $duration;
        $add->status = "need";
        $add->payment_method = "cash";
        $add->type = $request->input('type');
        $add->date = $request->input('date');
        $add->time = $request->input('time');
        $add->created_at = $dateTime;
        $add->save();
        
        $code = rand(1000,9999);
        
        $update_code = \App\Trip::where('id', $add->id)->update(['code' => $code]);
        
       $get_drivers = DB::select("SELECT users.id , car_name, plate, color, brand,sear_num, users.id as user_id, users.first_name, users.last_name, users.phone, users.gender, users.image, users.rate , ( 6371 * acos( cos( radians($start_lat) ) * cos( radians( `Latitude` ) ) * cos( radians( `Longitude` ) - radians('$start_long') ) + sin( radians('$start_lat') ) * sin( radians( `Latitude` ) ) ) ) AS distance FROM `users`
                            join user_car on users.id = user_car.user_id
                            where users.role = '3' and users.is_online = '1' and users.is_busy = '0' and user_car.baka_id = ? and users.gender =? and user_car.type_id= '1'  ORDER BY distance ASC limit 6",[$baka_id, $gend]);


        if( $get_drivers == NULL){
            $message['error'] = 1;
            $message['message'] = $msg_error3;
        }else{
            
            foreach($get_drivers as $driver){
             
                $send = new \App\Driver_trip;
                $send->trip_id = $add->id;
                $send->driver_id = $driver->user_id;
                $send->state = "wait";
                $send->created_at = $dateTime;
                $send->save();
                

                   
                 
                 
            }
        }
        
        if( $add == true && $send == true){

                foreach($get_drivers as $driver){
                  $get_userData = \App\User::where('id' , $driver->user_id )->first();

    
    
                        #prep the bundle
                            
                          $msg = array
                              (
                    		'body' 	=>  "توجد رحله جديدة بجوارك" ,
                    		'title'	=>  "Taxina Trip",
                            'click_action' => "1"
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_userData->firebase_token,
                                    'data' => $mm = array(
                                                 'redirect_id'  => "$add->id",
                                                 'type' => "1",
                                                 'status' => "1",
                                            ),
                    				'notification'	=> $msg
                    			);
                            		 
                        	                 
    
                        	$headers = array
                        			(
                        				'Authorization: key=' . API_ACCESS_KEY,
                        				'Content-Type: application/json'
                        			);
                        #Send Reponse To FireBase Server	
                        		$ch = curl_init();
                        		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        		curl_setopt( $ch,CURLOPT_POST, true );
                        		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        		$result = curl_exec($ch );
                        		//echo $result;
                        		curl_close( $ch );
                        		
                        		$add_notify = new \App\Notification;
                            	
                            	$add_notify->body = "توجد رحله جديدة بجوارك" ;
                            	$add_notify->title =  "Taxina Trip";
                            	$add_notify->click_action = 1;
                            	$add_notify->redirect_id = $add->id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 1;
                            	$add_notify->user_id = $driver->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                }



            $message['trip_id'] = $add->id;
            $message['error'] = 0;
            $message['message'] = $msg_data;
        }else{
            $message['error'] = 1;
            $message['message'] = $msg_error;
        }


        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
    }


    public function show_driver_trip(Request $request){

        if(auth()->User()){
            $msg_data ="";
            $msg_error ="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "بيانات السواق";
                    $msg_error  = "لم يتم قبول هذه الرحله من قبل السائق";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "Driver trip Data";
                    $msg_error = "Trip didn't accept yet";
                    $msg_token = "Token is not provided";   
                }

            $trip_id = $request->input('trip_id');

            $check = \App\Trip::where('id' , $trip_id)->value('status');
            $driver_id = \App\Trip::where('id' , $trip_id)->value('driver_id');

            if( $check == 'accept'){

                $get_data = \App\Trip::select( 'users.id', 'first_name', 'last_name','phone','users.rate','users.image',
                                                'user_car.car_name' ,'user_car.plate','trip.pick_time' , 'trip.code' )
                                    ->leftjoin('users' , 'trip.driver_id' ,'=' ,'users.id')
                                    ->leftjoin('user_car' , 'trip.userCar_id' ,'=' ,'user_car.id')
                                    ->where('driver_id' , $driver_id)->first();

                $message['data'] = $get_data;                    
                $message['error'] = 0;
                $message['message'] = $msg_data;

            }else{
                $message['data'] = NULL;                    
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
            
    }


    public function cancel_trip(Request $request){
        if( auth()->user()){
             $created_at = carbon::now()->toDateTimeString();
           $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

           $msg_data ="";
            $msg_error ="";
            $msg_error2 ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم إلغاء رحلتك";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "لقد تم قبول الرحله منقبل السائق";
                }else{
                    $msg_data = "Trip is canceled successfully";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 = "the driver is accepted the trip, you can't cancel ";   
                }

            $trip_id = $request->input('trip_id');
            
            $check = \App\Trip::where([['id' , $trip_id], ['status' , 'need']])->first();
            
            if( $check != NULL){
                $cancel = \App\Trip::where('id' , $trip_id)->update(['status' => "cancel"]);
    
                if( $cancel == true){
                    
                     $get_user = \App\Trip::select('user_id', 'driver_id')->where('id' , $trip_id)->first();
                     
                     $get_userData = \App\User::where('id' ,$get_user->user_id )->value('firebase_token');
                    
    
        
                            #prep the bundle
                                
                            	   $msg = array(
                                    		'body' 	=>  "تم إلغاء رحلتك نجاح" ,
                                    		'title'	=>  "Taxina Trip",
                                            'click_action' => "3"
                                              );
                                    	$fields = array
                                    			(
                                    				'to'		=> $get_userData,
                                                    'data' => $ms = array(
                                                                 'redirect_id'  => "$trip_id",
                                                                 'type' => "1",
                                                                 'status' => "3",
                                                            ),
                                    				'notification'	=> $msg
                                    			);
                                            	               
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                             
                             	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body =  "تم إلغاء رحلتك نجاح" ;
                            	$add_notify->title =  "Taxina Trip";	
                            	$add_notify->click_action = 3;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 3;
                            	$add_notify->user_id = $get_user->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            
                    
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    public function trip_payment(Request $request){

        if( auth()->user()){
            
              $created_at = carbon::now()->toDateTimeString();
            $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
           
            $msg_data ="";
            $msg_error ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "تم أنتهاء رحلتك";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                }else{
                    $msg_data = "Trip is ended successfully";
                    $msg_error = "there is an error, please try again";
                }

            $trip_id = $request->input('trip_id');
            $payment_method =  "cash";               //$request->input('payment_method');
            $paid = $request->input('paid');
            
            $get_TripPrice = \App\Trip::where('id' , $trip_id)->value('price');
                        
            $get_TripUser = \App\Trip::where('id' , $trip_id)->value('user_id');

            $user_wallet = \App\User::where('id' , $get_TripUser)->value('wallet');
           
            $driver_wallet = \App\User::where('id' , auth()->user()->id)->value('wallet');

            if($user_wallet == 0){
                
                $total = $paid - $get_TripPrice;
                
                if( $total != 0){
                    $user_total = $user_wallet + $total;
                    
                    $update_wallet = \App\User::where('id' ,  $get_TripUser)->update(['wallet' =>$user_total]);
                }
            }else{
                             
                $total = $paid - $get_TripPrice;
                
                $user_total = $user_wallet - $total;
                
                $update_wallet = \App\User::where('id' ,  $get_TripUser)->update(['wallet' =>$user_total]);

            }
            
            /********** add driver and company pressent *****/
            
            $company_wallet = \App\aboutUs::select('present' ,'wallet' )->where('id' , '1')->first();

            $company_money = ($get_TripPrice * $company_wallet->present) / 100 ;
            
            $company_total = $company_wallet->wallet + $company_money;
            
            
            $driver_money = $get_TripPrice - $company_money;
            
            $driver_total = $driver_wallet + $driver_money;
            
             
            /*********update wallet */
            
                      
            $update_company = \App\aboutUs::where('id' , '1')->update(['wallet' => $company_total]);
         
            $update_driver = \App\User::where('id' ,  auth()->user()->id)->update(['wallet' =>$driver_total]);

             
            $update = \App\Trip::where('id' , $trip_id)->update(['status' => "end" , 'payment_method' => $payment_method]);
            
            $update_driverbusy = \App\User::where('id' , auth()->User()->id)->update(['is_busy' => 0]);

            if( $update == true){
                
                 $user_firebase = \App\User::where('id' , $get_TripUser )->value('firebase_token');
                
    
                #prep the bundle
                    
                	  $msg = array(
                        		'body' 	=>  "تم أنهاء رحلتك نجاح" ,
                        		'title'	=>  "Taxina Trip",
                                'click_action' => "3"
                                  );
                        	$fields = array
                        			(
                        				'to'		=> $user_firebase,
                                        'data' => $mg = array(
                                                     'redirect_id'  => "$trip_id",
                                                     'type' => "1",
                                                     'status' => "3",
                                                ),
                        				'notification'	=> $msg
                        			);
                                    	  
                	                 

                	$headers = array
                			(
                				'Authorization: key=' . API_ACCESS_KEY,
                				'Content-Type: application/json'
                			);
                #Send Reponse To FireBase Server	
                		$ch = curl_init();
                		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                		curl_setopt( $ch,CURLOPT_POST, true );
                		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                		$result = curl_exec($ch );
                		//echo $result;
                		curl_close( $ch );
                		
                			$add_notify = new \App\Notification;
                            	
                        	$add_notify->body = "تم أنهاء رحلتك نجاح" ;
                        	$add_notify->title =  "Taxina Trip";
                        	$add_notify->click_action = 3;
                        	$add_notify->redirect_id = $trip_id;
                        	$add_notify->type = 1;
                        	$add_notify->status = 3;
                        	$add_notify->user_id = $get_TripUser;
                        	$add_notify->created_at = $dateTime;
                        	$add_notify->save();
                 
                
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
    }

    public function rate_trip(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";
            $msg_error2="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم تقيم الرحلة";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "منفضلك ادخل البييانات";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "Trip is rating";
                    $msg_error = "there is an error, please try again";
                    $msg_error2= "please enter all the fields";
                    $msg_token = "Token is not provided";   
                }

            $trip_id = $request->input('trip_id');
            $rate = $request->input('rate');

            if( $request->has('rate') && $rate != NULL && $request->has('trip_id') && $trip_id != NULL){
                $update = \App\Trip::where('id' , $trip_id)->update(["rate" => $rate]);
    
                if( $update == true){
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }else{
                $message['error'] = 1;
                $message['message'] =$msg_error2;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "Token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    
    
    public function add_car(Request $request){
        if(auth()->user()){
            
             $msg_data ="";
            $msg_error ="";
            $msg_error2="";
            $msg_token ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم أضافة العربية ،فى انتظار قبولها";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "انت بالفعل تملك سياره";
                    $msg_token = " من فضلك سجل الدخول";
                }else{
                    $msg_data = "add successfully, waiting acception";
                    $msg_error = "there is an error, please try again";
                    $msg_error2= "you already have a car";
                    $msg_token = "Token is not provided";   
                }
            
              $licence = "";
            
            if($request->has('driver_license')){
                $licence = $request->input('driver_license');
            }
            if($request->has('license_plate')){
                $licence = $request->input('license_plate');
            }
            
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
       
       
            $check = \App\User_car::where('user_id', auth()->User()->id)->value('id');
            
            if( $check != NULL){
                $message['error'] = 1;
                $message['message'] = $msg_error2;
                
            }else{
                $add =  new \App\User_car;
                
                $add->type_id = $request->input('type_id');
                $add->carCat_id = $request->input('carCat_id');
                $add->car_name = $request->input('car_name');
                $add->plate = $request->input('plate');
                $add->color = $request->input('color');
                $add->brand = $request->input('model');
                $add->year = $request->input('year');
                $add->driver_license = $licence;
                $add->status = "need";
                $add->user_id = auth()->user()->id;
                $add->created_at = $dateTime;
                $add->save();
                
                
                if( $add == true){
                    $message['data'] = \App\User::where('id', Auth()->User()->id)->first();
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['data'] = \App\User::where('id', Auth()->User()->id)->first();
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }            
        }else{
            $message['error'] = 2;
            $message['message'] = " من فضلك سجل الدخول";

        }
        return response()->json($message);
        
    }
    
    
    public function show_bakat(Request $request){
        if(auth()->User()){
            
            $msg_data ="";
            $msg_error ="";
            $data = array();
            
            $distance = $request->input('distance');
            
            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data =  "جميع الباقات";
                    $msg_error  = "لا يوجد باقات";
                }else{
                    $msg_data = "all bakat";
                    $msg_error = "No bakat";
                }
                
                
            $get_data = \App\Bakat::all();
            
            foreach( $get_data as $baka){
                
                $total = $distance * $baka->price;
                
                $alltotal = $total + $baka->start_price;
                
                array_push($data, (object)array(
                    "id" => $baka->id,
                    "name" => $baka->name,
                    "image" => $baka->image,
                    "price" =>$alltotal,
                    ));
                    
            }
            
            if( count($data) >0){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = $msg_data;
            }else{
                $mesage['data'] = $data;
                $message['error'] = 1;
                $message['message'] = $msg_error;
            }
            
        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
   
   
   //********************************* dash board ************************************************************************//
   public function show_allcarType(Request $request){
	
	     if(Auth()->User()){
	     	

	     $type = \App\Car_type::select('id','title','image')->get();

	     	if(count($type)>0){
     	 		
              $message['data']=$type;
              $message['error']=0;
               $message['message']='show all types';
     	 	}else{
              $message['data']=$type;
              $message['error']=1;
               $message['message']='no type yet';
     	 	}
     	 }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
        return response()->json($message);
	} 
    
    
    public function show_typeByid(Request $request){
	      if(Auth()->User()){


	         $id = $request->input('id');

	         $type = \App\Car_type::select('id','title','image')->where('id',$id)->first();


		     	if( $type !=null){
	     	 		
	              $message['data']=$type;
	              $message['error']=0;
	               $message['message']='show  type';
	     	 	}else{
	              $message['data']=NULL;
	              $message['error']=1;
	               $message['message']='no type yet';
	     	 	}
            }else{

          	    $message['error'] = 2;
                $message['message'] = 'this token is not provided';
            }
            return response()->json($message); 
	}
	
	
	 public function delete_type(Request $request){
	  if(Auth()->User()){

	         $id=$request->input('id');

	         $type = \App\Car_type::where('id',$id)->delete();

	     	if($type == true){
     	 	    $message['error']=0;
                $message['message']='delete type success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in delete type';
     	 	}
         }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
       return response()->json($message);  
	}
	
	
	public function insert_Cartype(Request $request){
	   if(Auth()->User()){

	      $title=$request->input('title');
	      $image=$request->file('image');
  
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
    
            
          if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                $destinationPath_id = 'uploads';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/".$savedFileName;
            }else{
                    $images = "";
            }
          
          $select= \App\Car_type::where('title',$title)->first();

          if($select !=null){
              
               $message['error']=4;
               $message['message']=' Car type is already exist';
              
          }else{
	         
	         $insert=new \App\Car_type;
	         $insert->title=$title;
	         $insert->image=$images;
	         $insert->created_at=$dateTime;
	         $insert->save();
	         
	     	if($insert == true){
     	 		
              $message['error']=0;
               $message['message']='insert type success';
     	 	}else{
              $message['error']=1;
               $message['message']='error in insert type';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
            return response()->json($message);
	}
	
	public function update_type(Request $request){
	  
	     if(Auth()->User()){

	       $id    = $request->input('id');  
	       $title = $request->input('title');
	       $image = $request->file('image');
     
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

            $check_img =\App\Car_type::where('id',$id)->value('image');
            
          if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                $destinationPath_id = 'uploads';
                $image->move($destinationPath_id, $savedFileName);

                $images = "http://taxiApi.codecaique.com/uploads/".$savedFileName;
            }else{
               if( $check_img!= NULL){
                  $images = $check_img;
               }else{
                  $images = "";
               }
            }
          
          $select = \App\Car_type::where([['title',$title],['id','!=',$id]])->first();

          if($select !=null){
              
               $message['error']=4;
               $message['message']=' type exist';
              
          }else{
	         
	         $update = \App\Car_type::where('id',$id)->update([
                                                'title'=>$title,
                                                'image'=>$images,
                                                'updated_at'=>$dateTime
                                
                                	          ]);
	        
	     	if($update == true){
     	 		
              $message['error']=0;
               $message['message']='update type success';
     	 	}else{
              $message['error']=1;
               $message['message']='error in update type';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
            return response()->json($message);
	}
    
    
    
    public function show_carCategory(Request $request){
	    if(Auth()->User()){
	     	$id=$request->input('type_id');

	       $category=Car_category::select('id','name','created_at')->where('type_id',$id)->get();

	     	if(count($category)>0){
     	 		
              $message['data']=$category;
              $message['error']=0;
               $message['message']='show all categorys';
     	 	}else{
              $message['data']=$category;
              $message['error']=1;
               $message['message']='no category yet';
     	 	}
     	 }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
        return response()->json($message);
	}
    
    
  
  public function show_categoryByid(Request $request){
	  if(Auth()->User()){
   
           $id=$request->input('id');

	         $category=Car_category::select('id','name')->where('id',$id)->first();


		     	if( $category !=null){
	     	 		
	              $message['data']=$category;
	              $message['error']=0;
	               $message['message']='show  category';
	     	 	}else{
	              $message['data']=NULL;
	              $message['error']=1;
	               $message['message']='no category yet';
	     	 	}
            }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
            }
          return response()->json($message);
	      
	}  
	
	
	
	public function delete_category(Request $request){
	  if(Auth()->User()){

	         $id=$request->input('id');

	         $category=Car_category::where('id',$id)->delete();

	     	if($category == true){
     	 	    $message['error']=0;
                $message['message']='delete category success';
     	 	}else{
               $message['error']=1;
               $message['message']='error in category type';
     	 	}
         }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }
     return response()->json($message);
	   
	}
	
	
	public function insert_category(Request $request){
	  if(Auth()->User()){

	       $name=$request->input('name');
	       $type_id =$request->input('type_id');
    
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
  
          $select=Car_category::where([['name',$name], ['type_id', $type_id]])->first();

          if($select !=null){
              
               $message['error']=4;
               $message['message']=' category exist';
              
          }else{
	         
	         $insert=new Car_category;
	         $insert->name=$name;
	         $insert->type_id=$type_id;
	         $insert->created_at=$dateTime;
	         $insert->save();
	         
	     	if($insert == true){
     	 		
              $message['error']=0;
               $message['message']='insert category success';
     	 	}else{
              $message['error']=1;
               $message['message']='error in insert category';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
      return response()->json($message);
	       
	}
	
	
	
	public function update_category(Request $request){
	  if(Auth()->User()){

	          
	      $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
            
            $name = $request->input('name');
            $id = $request->input('id');
            
          $select=Car_category::where([['name',$name],['id','!=',$id]])->first();

          if($select !=null){
              
               $message['error']=4;
               $message['message']='category exist';
              
          }else{
	         
	         $update=Car_category::where('id',$id)->update([
                'name'=>$name,
                'updated_at'=>$dateTime

	          ]);
	        
	     	if($update == true){
     	 		
              $message['error']=0;
               $message['message']='update category success';
     	 	}else{
              $message['error']=1;
               $message['message']='error in update category';
     	 	}
          }
        }else{

      	    $message['error'] = 2;
            $message['message'] = 'this token is not provided';
         }  
    return response()->json($message);
	       
	}
    
    
    
    
    
    public function driver_request_cancel_trip(Request $request){
        
        
            
        
        if( auth()->user()){
            $created_at = carbon::now()->toDateTimeString();
            $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $msg_data ="";
            $msg_error ="";
            $msg_error2 ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم ارسال طلبك";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "تم قبول الرحلة من قبل السائق";
                }else{
                    $msg_data = "Trip canceling requested";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 = "the driver is accepted the trip, you can't cancel ";   
                }

            $trip_id = $request->input('trip_id');
            
            $check = \App\Trip::where('id',$trip_id)->first();
            
            if( $check != NULL){
                
    
                    
                     $get_user = \App\Trip::select('user_id', 'driver_id')->where('id' , $trip_id)->first();
                     
                     $get_userData = \App\User::where('id' ,$get_user->user_id )->value('firebase_token');
                    
    
        
                            #prep the bundle
                                
                            	   $msg = array(
                                    		'body' 	=>  "تم ارسال طلب الغاء رحلتك" ,
                                    		'title'	=>  "Taxina Trip",
                                            'click_action' => "3"
                                              );
                                    	$fields = array
                                    			(
                                    				'to'		=> $get_userData,
                                                    'data' => $ms = array(
                                                                 'redirect_id'  => "$trip_id",
                                                                 'type' => "1",
                                                                 'status' => "3",
                                                            ),
                                    				'notification'	=> $msg
                                    			);
                                            	               
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                             
                             	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body =  "تم إلغاء رحلتك نجاح" ;
                            	$add_notify->title =  "Taxina Trip";	
                            	$add_notify->click_action = 3;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 3;
                            	$add_notify->user_id = $get_user->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            
                    
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                
                
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
        
        
    }
    
    public function driver_accept_cancel_trip(Request $request){
        if( auth()->user()){
            $created_at = carbon::now()->toDateTimeString();
            $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $msg_data ="";
            $msg_error ="";
            $msg_error2 ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم إلغاء رحلتك";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "لقد تم قبول الرحله منقبل السائق";
                }else{
                    $msg_data = "Trip is canceled successfully";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 = "the driver is accepted the trip, you can't cancel ";   
                }

            $trip_id = $request->input('trip_id');
            $accept = $request->input('accept');
            
            
            $check = \App\Trip::where('id' , $trip_id)->first();
            
            if($accept == false ||$accept == "false" ){
                
                    
                     $get_user = \App\Trip::select('user_id', 'driver_id')->where('id' , $trip_id)->first();
                     
                     $get_userData = \App\User::where('id' ,$get_user->user_id )->value('firebase_token');
                    
    
        
                            #prep the bundle
                                
                            	   $msg = array(
                                    		'body' 	=>  "تم إلغاء رحلتك نجاح" ,
                                    		'title'	=>  "Taxina Trip",
                                            'click_action' => "3"
                                              );
                                    	$fields = array
                                    			(
                                    				'to'   => $get_userData,
                                                    'data' => $ms = array(
                                                                 'redirect_id'  => "$trip_id",
                                                                 'type' => "1",
                                                                 'status' => "3",
                                                            ),
                                    				'notification'	=> $msg
                                    			);
                                            	               
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                             
                             	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body =  "تم إلغاء رحلتك نجاح" ;
                            	$add_notify->title =  "Taxina Trip";	
                            	$add_notify->click_action = 3;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 3;
                            	$add_notify->user_id = $get_user->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            
                    
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
            }
            else if( $check != NULL){
                $cancel = \App\Trip::where('id' , $trip_id)->update(['status' => "cancel"]);
    
                if( $cancel == true){
                    
                     $get_user = \App\Trip::select('user_id', 'driver_id')->where('id' , $trip_id)->first();
                     
                     $get_userData = \App\User::where('id' ,$get_user->user_id )->value('firebase_token');
                    
    
        
                            #prep the bundle
                                
                            	   $msg = array(
                                    		'body' 	=>  "تم إلغاء رحلتك نجاح" ,
                                    		'title'	=>  "Taxina Trip",
                                            'click_action' => "3"
                                              );
                                    	$fields = array
                                    			(
                                    				'to'   => $get_userData,
                                                    'data' => $ms = array(
                                                                 'redirect_id'  => "$trip_id",
                                                                 'type' => "1",
                                                                 'status' => "3",
                                                            ),
                                    				'notification'	=> $msg
                                    			);
                                            	               
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                             
                             	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body =  "تم إلغاء رحلتك نجاح" ;
                            	$add_notify->title =  "Taxina Trip";	
                            	$add_notify->click_action = 3;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 3;
                            	$add_notify->user_id = $get_user->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            
                    
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    
    
    public function user_request_cancel_trip(Request $request){
        if( auth()->user()){
            $created_at = carbon::now()->toDateTimeString();
            $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $msg_data ="";
            $msg_error ="";
            $msg_error2 ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم ارسال طلبك";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "لقد تم قبول الرحله منقبل السائق";
                }else{
                    $msg_data = "Trip is canceled successfully";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 = "the driver is accepted the trip, you can't cancel ";   
                }

            $trip_id = $request->input('trip_id');
            
            $check = \App\Trip::where('id' , $trip_id)->first();
            
            if( $check != NULL){
                
    
                    
                     $get_user = \App\Trip::select('user_id', 'driver_id')->where('id' , $trip_id)->first();
                     
                     $get_userData = \App\User::where('id' ,$get_user->driver_id )->value('firebase_token');
                    
    
        
                            #prep the bundle
                                
                            	   $msg = array(
                                    		'body' 	=>  "تم إلغاء رحلتك نجاح" ,
                                    		'title'	=>  "Taxina Trip",
                                            'click_action' => "3"
                                              );
                                    	$fields = array
                                    			(
                                    				'to'		=> $get_userData,
                                                    'data' => $ms = array(
                                                                 'redirect_id'  => "$trip_id",
                                                                 'type' => "1",
                                                                 'status' => "3",
                                                            ),
                                    				'notification'	=> $msg
                                    			);
                                            	               
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                             
                             	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body =  "تم إلغاء رحلتك نجاح" ;
                            	$add_notify->title =  "Taxina Trip";	
                            	$add_notify->click_action = 3;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 3;
                            	$add_notify->user_id = $get_user->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            
                    
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                
                
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    public function user_accept_cancel_trip(Request $request){
        if( auth()->user()){
            $created_at = carbon::now()->toDateTimeString();
            $dateTime= date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

            $msg_data ="";
            $msg_error ="";
            $msg_error2 ="";

            $check_setting = \App\Setting::where('user_id' , auth()->User()->id)->value('language');
                
                if($check_setting == 'ar'){
                    $msg_data = "تم إلغاء رحلتك";
                    $msg_error  = "يوجد خطأ، حاول مره أخرى";
                    $msg_error2 = "لقد تم قبول الرحله منقبل السائق";
                }else{
                    $msg_data = "Trip is canceled successfully";
                    $msg_error = "there is an error, please try again";
                    $msg_error2 = "the driver is accepted the trip, you can't cancel ";   
                }

            $trip_id = $request->input('trip_id');
            

            
            
            $accept = $request->input('accept');
            
            
            $check = \App\Trip::where('id' , $trip_id)->first();
            
            if($accept == false ||$accept == "false" ){
                
                    
                     $get_user = \App\Trip::select('user_id', 'driver_id')->where('id' , $trip_id)->first();
                     
                     $get_userData = \App\User::where('id' ,$get_user->user_id )->value('firebase_token');
                    
    
        
                            #prep the bundle
                                
                            	   $msg = array(
                                    		'body' 	=>  "تم إلغاء رحلتك نجاح" ,
                                    		'title'	=>  "Taxina Trip",
                                            'click_action' => "3"
                                              );
                                    	$fields = array
                                    			(
                                    				'to'   => $get_userData,
                                                    'data' => $ms = array(
                                                                 'redirect_id'  => "$trip_id",
                                                                 'type' => "1",
                                                                 'status' => "3",
                                                            ),
                                    				'notification'	=> $msg
                                    			);
                                            	               
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                             
                             	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body =  "تم إلغاء رحلتك نجاح" ;
                            	$add_notify->title =  "Taxina Trip";	
                            	$add_notify->click_action = 3;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 3;
                            	$add_notify->user_id = $get_user->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            
                    
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
            }
            else if( $check != NULL){
                $cancel = \App\Trip::where('id' , $trip_id)->update(['status' => "cancel"]);
    
                if( $cancel == true){
                    
                     $get_user = \App\Trip::select('user_id', 'driver_id')->where('id' , $trip_id)->first();
                     
                     $get_userData = \App\User::where('id' ,$get_user->driver_id )->value('firebase_token');
                    
    
        
                            #prep the bundle
                                
                            	   $msg = array(
                                    		'body' 	=>  "تم إلغاء رحلتك نجاح" ,
                                    		'title'	=>  "Taxina Trip",
                                            'click_action' => "3"
                                              );
                                    	$fields = array
                                    			(
                                    				'to'		=> $get_userData,
                                                    'data' => $ms = array(
                                                                 'redirect_id'  => "$trip_id",
                                                                 'type' => "1",
                                                                 'status' => "3",
                                                            ),
                                    				'notification'	=> $msg
                                    			);
                                            	               
        
                            	$headers = array
                            			(
                            				'Authorization: key=' . API_ACCESS_KEY,
                            				'Content-Type: application/json'
                            			);
                            #Send Reponse To FireBase Server	
                            		$ch = curl_init();
                            		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            		curl_setopt( $ch,CURLOPT_POST, true );
                            		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            		$result = curl_exec($ch );
                            		//echo $result;
                            		curl_close( $ch );
                             
                             	$add_notify = new \App\Notification;
                            	
                            	$add_notify->body =  "تم إلغاء رحلتك نجاح" ;
                            	$add_notify->title =  "Taxina Trip";	
                            	$add_notify->click_action = 3;
                            	$add_notify->redirect_id = $trip_id;
                            	$add_notify->type = 1;
                            	$add_notify->status = 3;
                            	$add_notify->user_id = $get_user->user_id;
                            	$add_notify->created_at = $dateTime;
                            	$add_notify->save();
                            
                    
                    $message['error'] = 0;
                    $message['message'] = $msg_data;
                }else{
                    $message['error'] = 1;
                    $message['message'] = $msg_error;
                }
            }else{
                $message['error'] = 1;
                $message['message'] = $msg_error2;
            }

        }else{
            $message['error'] = 2;
            $message['message'] = "token is not provided";

        }
        return response()->json($message);
    }
    
    

}
