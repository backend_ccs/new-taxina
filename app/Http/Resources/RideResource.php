<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;



class RideResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'driver_id' => $this->driver_id,
            'start_lat' => $this->start_lat,
            'start_long' => $this->start_long,
            'start_location' => $this->start_location,
            'end_lat' => $this->end_lat,
            'end_long' => $this->end_long,
            'end_location' => $this->end_location,
            'distance' => $this->distance,
            'car_type_id' => $this->car_type_id,
            'promo_code' => $this->promo_code,
            'price' => $this->price,
            'note' => $this->note,
            'duration_time' => $this->duration_time,
            'rate' => $this->rate,
            'user_rate' => $this->user_rate,
            'driver_rate' => $this->driver_rate,
            'status' => $this->status,
            'pick_time' => $this->pick_time,
            'code' => $this->code,
            'payment_method' => $this->payment_method,
            'type' => $this->type,
            'date' => $this->date,
            'time' => $this->time,
        ];
    }
}
