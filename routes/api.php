<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json; charset=UTF-8', true); 
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('delete_phone' , 'userController@delete_phone');

Route::post('user_registration' ,'userController@user_registration');

Route::post('add_users' ,'userController@add_users');

Route::post('user_login' ,'userController@user_login');

Route::post('social_login' ,'userController@social_login');

Route::get('privacy_policy' ,'userController@privacy_policy');

Route::get('check_phone' ,"userController@check_phone");


Route::group([
    'middleware' => 'jwt.auth'
    // 'prefix' => 'admin'
], function($router) {
    
    Route::get('show_user_ByID' ,"userController@show_user_ByID");
    Route::post('update_profile' ,"userController@update_profile");
    Route::post('add_favorite_place' ,"userController@add_favorite_place");
    Route::get('show_userFavorite_place' ,"userController@show_userFavorite_place");
    Route::get('show_userByID' ,"userController@show_userByID");

    Route::get('counts' ,"adminController@counts");

    Route::get('show_carType' ,"carController@show_carType");
    Route::get('get_suggested_car' ,"carController@get_suggested_car");
    // Route::get('show_userCar' ,"carController@show_userCar");


    Route::get('show_promotions' ,"copounController@show_promotions");
    Route::get('check_promoCode' ,"copounController@check_promoCode");

    Route::get('show_bakat' ,"carController@show_bakat");

    Route::post('make_trip' ,"carController@make_trip");
    Route::get('show_driver_trip' ,"carController@show_driver_trip");
    Route::get('cancel_trip' ,"carController@cancel_trip");
    
    Route::get('driver_request_cancel_trip' ,"carController@driver_request_cancel_trip");
    Route::get('driver_accept_cancel_trip' ,"carController@driver_accept_cancel_trip");

    Route::get('user_request_cancel_trip' ,"carController@user_request_cancel_trip");
    Route::get('user_accept_cancel_trip' ,"carController@user_accept_cancel_trip");
    
    Route::get('rate_trip' ,"carController@rate_trip");

    Route::get('rate_specialTrip' ,"specialTripController@rate_specialTrip");

    Route::get('search_trip' ,"specialTripController@search_trip");


    Route::get('show_specialTrip' ,"specialTripController@show_specialTrip");
    Route::get('show_trip_seats' ,"specialTripController@show_trip_seats");
    Route::get('join_trip' ,"specialTripController@join_trip");

    Route::get('User_complete_trip' ,"tripController@User_complete_trip");
    Route::get('User_upcoming_trip' ,"tripController@User_upcoming_trip");
    Route::get('User_waiting_trip' ,"tripController@User_waiting_trip");

    
    Route::get('User_canceled_trip' ,"tripController@User_canceled_trip");
    Route::get('User_state_trip' ,"tripController@User_state_trip");

    Route::post('change_passward' ,"userController@change_passward");

    Route::get('show_area_city' ,"positionController@show_area_city");

    Route::get('show_trip_cost' ,"tripController@show_trip_cost");

    Route::get('forget_password' ,"userController@forget_password");

    Route::post('Reset_password' ,"userController@Reset_password");
  
    Route::get('add_user_country' ,"placeController@add_user_country");


    Route::get('delete_driver' ,"userController@delete_driver");
    Route::get('delete_favorite' ,"userController@delete_favorite");


});




Route::group([
    'middleware' => 'jwt.auth'
    //'prefix' => 'driver'
], function($router) {

    
    Route::post('Driver_registration' ,"userController@Driver_registration");
    Route::post('Driver_login' ,"userController@Driver_login");
    Route::get('change_language' ,"userController@change_language");
    Route::get('online_offline' ,"userController@online_offline");
    Route::get('update_location' ,"userController@update_location");

    Route::get('user_wallet' ,"userController@user_wallet");

    Route::post('add_specialtrip' ,"specialTripController@add_specialtrip");
    Route::get('start_specialTrip' ,"specialTripController@start_specialTrip");
    Route::get('end_specialTrip' ,"specialTripController@end_specialTrip");

    Route::get('Block_trip' ,"specialTripController@Block_trip");
    Route::get('show_users_specialTrip' ,"specialTripController@show_users_specialTrip");
    Route::get('show_user_seats' ,"specialTripController@show_user_seats");
    Route::get('show_tripUser_seats' ,"specialTripController@show_tripUser_seats");
    Route::get('search_specialTrip' ,"specialTripController@search_specialTrip");

    
    Route::get('show_UpComingdriver_specialTrip' ,"specialTripController@show_UpComingdriver_specialTrip");
    
    Route::get('show_UpComingdriver_Trip' ,"tripController@show_UpComingdriver_Trip");

    
    Route::get('show_driver_completedtrip' ,"tripController@show_driver_completedtrip");

    Route::get('upcomingDriver_Bus' ,"specialTripController@upcomingDriver_Bus");
    Route::get('completedDriver_Bus' ,"specialTripController@completedDriver_Bus");


    Route::get('show_specialTrip_need' ,"specialTripController@show_specialTrip_need");
    Route::get('accept_specialTrip' ,"specialTripController@accept_specialTrip");
    Route::get('Refused_specialTrip' ,"specialTripController@Refused_specialTrip");


    Route::get('accept_tripSeats' ,"specialTripController@accept_tripSeats");
    Route::get('refuse_tripSeats' ,"specialTripController@refuse_tripSeats");
  

    Route::post('add_car' ,"carController@add_car");
    Route::get('show_suggested_car' ,"carController@show_suggested_car");


    Route::get('Driver_requested_trip' ,"tripController@Driver_requested_trip");
    Route::get('accept_trip' ,"tripController@accept_trip");
    Route::get('trip_payment' ,"carController@trip_payment");

    Route::get('show_urTrip_nw' ,"tripController@show_urTrip_nw");



    Route::get('show_all_city' ,"positionController@show_all_city");
    Route::get('show_area_ByID' ,"positionController@show_area_ByID");


    Route::get('show_trip_ByID' ,"tripController@show_trip_ByID");

    Route::get('my_notification' ,"userController@my_notification");

    Route::get('show_tripDetails_ById' ,"tripController@show_tripDetails_ById");

});



Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'admin'
], function($router) {

    Route::post('admin_login' ,"adminController@admin_login");
    Route::post('update_admin_profile' ,"adminController@update_admin_profile");
    Route::post('update_usersData' ,"userController@update_usersData");

    Route::get('show_all_users' ,"adminController@show_all_users");
    Route::get('show_all_drivers' ,"adminController@show_all_drivers");
    Route::get('show_drivers_refusedcar' ,"adminController@show_drivers_refusedcar");
    Route::get('show_drivers_needcar' ,"adminController@show_drivers_needcar");

    Route::get('show_aboutUS' ,"adminController@show_aboutUS");
    Route::post('update_aboutUS' ,"adminController@update_aboutUS");


    Route::get('show_driverCar' ,"userController@show_driverCar");
    Route::get('accept_Usercar' ,"userController@accept_Usercar");
    Route::get('refused_Usercar' ,"userController@refused_Usercar");
    Route::post('update_userTarget' ,"userController@update_userTarget");
    
    Route::get('show_driver_bus' ,"userController@show_driver_bus");


});


Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'carType'
], function($router) {

    Route::get('show_allcarType' ,"carController@show_allcarType");
    Route::get('show_typeByid' ,"carController@show_typeByid");
    Route::get('delete_type' ,"carController@delete_type");
    Route::post('insert_Cartype' ,"carController@insert_Cartype");
    Route::post('update_type' ,"carController@update_type");


});



Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'category'
], function($router) {

    Route::get('show_carCategory' ,"carController@show_carCategory");
    Route::get('show_categoryByid' ,"carController@show_categoryByid");
    Route::get('delete_category' ,"carController@delete_category");
    Route::post('insert_category' ,"carController@insert_category");
    Route::post('update_category' ,"carController@update_category");


});


Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'country'
], function($router) {

    Route::get('show_country' ,"placeController@show_country");
    Route::get('show_countryByid' ,"placeController@show_countryByid");
    Route::get('delete_country' ,"placeController@delete_country");
    Route::post('insert_country' ,"placeController@insert_country");
    Route::post('update_country' ,"placeController@update_country");


});

Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'city'
], function($router) {
    
    Route::get('show_allcities' ,"placeController@show_allcities");
    Route::get('show_cities' ,"placeController@show_cities");
    Route::get('show_cityByid' ,"placeController@show_cityByid");
    Route::get('delete_cities' ,"placeController@delete_cities");
    Route::post('insert_city' ,"placeController@insert_city");
    Route::post('update_city' ,"placeController@update_city");


});



Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'area'
], function($router) {

    Route::get('show_Area' ,"placeController@show_Area");
    Route::get('show_areaByid' ,"placeController@show_areaByid");
    Route::get('delete_area' ,"placeController@delete_area");
    Route::post('insert_area' ,"placeController@insert_area");
    Route::post('update_area' ,"placeController@update_area");


});




Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'offers'
], function($router) {

    Route::get('show_alloffers' ,"copounController@show_alloffers");
    Route::get('show_offerByid' ,"copounController@show_offerByid");
    Route::get('delete_offer' ,"copounController@delete_offer");
    Route::post('insert_offer' ,"copounController@insert_offer");
    Route::post('update_offer' ,"copounController@update_offer");


    Route::get('show_user_offers' ,"OffersController@show_user_offers");
    Route::get('delete_user_offer' ,"OffersController@delete_user_offer");
    Route::get('show_user_offerbyid' ,"OffersController@show_user_offerbyid");
    Route::post('insert_user_offer' ,"OffersController@insert_user_offer");
    Route::get('close_user_offer' ,"OffersController@close_user_offer");

 

});



Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'bakat'
], function($router) {

    Route::get('show_allbakat' ,"copounController@show_allbakat");
    Route::get('show_bakaByid' ,"copounController@show_bakaByid");
    Route::get('delete_baka' ,"copounController@delete_baka");
    Route::post('insert_baka' ,"copounController@insert_baka");
    Route::post('update_baka' ,"copounController@update_baka");


});




Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'trip'
], function($router) {

    Route::get('show_needTrip' ,"adminController@show_needTrip");
    Route::get('show_acceptTrip' ,"adminController@show_acceptTrip");
    Route::get('show_endTrip' ,"adminController@show_endTrip");


});

Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'driver'
], function($router) {

    Route::post('add_driver' ,"userController@add_driver");
    Route::post('add_drivercar' ,"userController@add_drivercar");
    Route::post('update_drivercar' ,"userController@update_drivercar");


});




Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'special'
], function($router) {

    Route::get('show_allendspecialtrip' ,"specialTripController@show_allendspecialtrip");
    Route::get('show_needspecialtrip' ,"specialTripController@show_needspecialtrip");
    Route::get('show_refusedspecialtrip' ,"specialTripController@show_refusedspecialtrip");
    Route::get('delete_specialtrip' ,"specialTripController@delete_specialtrip");
    Route::post('new_specialtrip' ,"specialTripController@new_specialtrip");

  

});






Route::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'family'
], function($router) {

    Route::get('search_phone' ,"familyController@search_phone");
    Route::post('send_familyRequest' ,"familyController@send_familyRequest");
    Route::get('show_familyMembers_accept' ,"familyController@show_familyMembers_accept");
    Route::get('show_familyMembers_wait' ,"familyController@show_familyMembers_wait");

    Route::post('accept_request' ,"familyController@accept_request");


});



//  common (client & driver)
// about us
Route::get('aboutus' , 'AboutUsController@index');
Route::get('aboutus/{id}','AboutUsController@show');
Route::put('aboutus' , 'AboutUsController@edit');
Route::patch('aboutus/{id}' , 'AboutUsController@update');


// general
    Route::get('ride/{id}' , 'RideController@single');

//client
    //get types of vhiecles
    Route::get('vhieclesTypes' , 'VhiecleController@index');

    //rides
    Route::post('userrequestride' , 'RideController@userRequestRide');

    Route::post('ridestatus' , 'RideController@ridestatus');
    Route::post('rideuserrate' , 'RideController@rideUserRate');

    // history
    Route::get('userrides/{id}' , 'RideController@userRides');


    Route::delete('ride/{id}' , 'RideController@delete');

    Route::get('history' , 'RideController@showHistory');
    
    // accept or reject driver
    Route::post('rideaction' , 'RideController@rideaction');

    // rate
    Route::post('ratedriver' , 'RateController@ratedriver');

    // report problem
    Route::post('reportproblem' , 'RateController@reportProblem');


// driver

    Route::post('ridedriverrate' , 'RideController@rideDriverRate');

    Route::get('driverrides/{id}' , 'RideController@driverRides');


    Route::get('riderequest/{id}' , 'RideRequestController@showRideRequest');
    Route::get('singleriderequest/{id}' , 'RideRequestController@singleRideRequest');

    Route::post('createRideRequest' , 'RideRequestController@createRideRequest');

    Route::post('statusUserRideRequest' , 'RideRequestController@statusUserRideRequest');
    Route::post('statusDriverTripRequest' , 'RideRequestController@statusDriverTripRequest');



    